// url to send the request to
var wsurl = 'wss://cbs.silkretail.com:8888/ws'; // spremenjeno iz 8888
var zabbix_link = 'https://zabbix.egaming.systems/index.php';
var kibana_link = 'http://77.234.149.13:5601/goto/82a072e1b2d229fe1cdbcbb5c24ffd3e';
var rundeck_link = 'http://10.100.2.2:4440/user/login';
var video_link = 'https://terminator.silkretail.com:4555/Video/';
var date_folder = $.datepicker.formatDate('yy-mmdd', new Date());
var terminal_name_exist = false; // needed for cheking existing values!
var session_update = false; // za subscribe to chanel
var localSession = {};
var notification = "";
// init function
$(function () {
    // include view files
    $.each($('[data-include]'), function(){
        $(this).load('views/' + $(this).data('include') + '.html', function() {
            if($(this).data('include') == 'footer'){
                // generate aside links
                $('#zabbix_link').attr('href', zabbix_link);
                $('#kibana_link').attr('href', kibana_link);
                $('#rundeck_link').attr('href', rundeck_link);
                $('#videos_link').attr('href', video_link + date_folder +'/');
                // user data
                $(".user-name").html(localStorage.login_name);
                // active menu link
                currently_active();
            }
        });
    });

    // dissmis alert
    $("div.alert").on("click", "button.close", function() {
        $(".slideup-alert").slideUp(500, function() {
            $(this).hide();
        });
    });

    // user logout
    $(document).on("click", "#signOut", function(){
        // Logout User
        localStorage.setItem('login_uid', '');
        localStorage.setItem('login_name', '');
        localStorage.setItem('wamp_username', '');
        localStorage.setItem('wamp_pwd', '');
    });

});

// if any param is empty
function empty(mixed_var) {
    var undef, key, i, len;
    var emptyValues = [undef, null, false, ""];
    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }
    return false;
}

// copy to clipboard
function copyToClipboard(vnc) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(vnc).select();
    document.execCommand("copy");
    $temp.remove();
}

// set aside menu as active
function currently_active(){
    var url = location.pathname;
    $("li[data-href]").each(function(){
        if (url.indexOf($(this).data("href")) > -1) {
            $("li[data-href='"+$(this).data("href")+"']").addClass("active");
        }
    });
}

// Read url params function
function url_param(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return !(results === null) ? results[1] : name+' has no data.';
}

// alert notification
function alertNotification(notification) {
    var alert = '';
    alert += '<div class="alert alert-danger alert-dismissible slideup-alert fade in">';
    alert += '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    alert += '  <h4><i class="icon fa fa-warning"></i> Alert!</h4>';
    alert += notification;
    alert += '</div>';
    $('#responseNotification').html('');
    $('#alertNotification').html(alert);
    $("#alertNotification").scrollIntoView();
}
// action notification
function actionNotification(notification) {
    var alert = '';
    alert += '<div class="alert alert-warning alert-dismissible slideup-alert fade in">';
    alert += '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    alert += '  <h4><i class="icon fa fa-info"></i> Info!</h4>';
    alert += notification;
    alert += '</div>';

    $('#actionNotification').html(alert);
}

// response notification
function responseNotification(notification) {
    var alert = '';
    alert += '<div class="alert alert-info alert-dismissible slideup-alert fade in">';
    alert += '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    alert += '  <h4><i class="icon fa fa-check"></i> Info!</h4>';
    alert += notification;
    alert += '</div>';

    $('#alertNotification').html('');
    $('#responseNotification').html(alert);
}

// rpcCall
function rpcCall(session, call, args) {
    var sessionOpen = session.isOpen;
    session.call(call, args).then(
        function (data) {
            if (data != "" && sessionOpen) {
                responseNotification('Function "' + call + '" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>');
            } else {
                alertNotification('No data is recived from function "' + call + '".');
            }
        },
        function (error) {
            alertNotification('RPC function "' + call + '" returned error: <b>' + error.error + '</b>');
            console.log(error.error);
        }
    );
}

// add frontend session
function addFrontendSession(session, args) {
    var sessionOpen = session.isOpen;
    session.call('si.nazvezi.cbs.add_frontend_client', args).then(
        function (data) {
            if (data != "" && sessionOpen) {
                console.log(data);
            } else {
                console.log(data);
            }
        },
        function (error) {
            console.log(error);
        }
    );
}

// Vampire rpc call
function vampire_start_proc(session, terminal, args) {
    var sessionOpen = session.isOpen;
    session.call("si.nazvezi.listener.send_to_watchers",[{"function":"start_proc", "args":args, "kwargs":{}}, terminal]).then(
        function (data) {
            if (data != "" && sessionOpen) {
                //return 'Function "si.nazvezi.watcher.' + terminal + '.start_proc" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>';
                var output = "";
                $.each(terminal, function(index, termin) {
                    output += "Terminal " + termin + " returned: " + JSON.stringify(data[termin].output[0]) + "<br>";
                });
                responseNotification('Function "si.nazvezi.watcher.' + terminal + '.start_proc" recived data: <blockquote>' + output + '</blockquote>');
            } else {
                //return 'No data is recived from function "si.nazvezi.watcher.' + terminal + '.start_proc".';
                alertNotification('No data is recived from function "si.nazvezi.watcher.' + terminal + '.start_proc".');
            }
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.watcher.' + terminal + '.start_proc" returned error: <b>' + error.error + '</b>');
            console.log(error.error);
        }
    );
}

function vampire_stop_proc(session, terminal, args) {
    var sessionOpen = session.isOpen;
    session.call("si.nazvezi.listener.send_to_watchers",[{"function":"stop_proc", "args":args, "kwargs":{}}, terminal]).then(
        function (data) {
            if (data != "" && sessionOpen) {
                var output = "";
                $.each(terminal, function(index, termin) {
                    output += "Terminal " + termin + " returned: " + JSON.stringify(data[termin].output[0]) + "<br>";
                });
                responseNotification('Function "si.nazvezi.watcher.' + terminal + '.stop_proc" recived data: <blockquote>' + output + '</blockquote>');
            } else {
                //return 'No data is recived from function "si.nazvezi.watcher.' + terminal + '.start_proc".';
                alertNotification('No data is recived from function "si.nazvezi.watcher.' + terminal + '.stop_proc".');
            }
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.watcher.' + terminal + '.stop_proc" returned error: <b>' + error.error + '</b>');
            console.log(error.error);
        }
    );
}
function watcher_run(session, args){
    session.call('si.nazvezi.telescope.ssh_terminal',args).then(
        function (data) {
            if (data != "" && sessionOpen) {
                responseNotification('Function "si.nazvezi.telescope.ssh_terminal" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>');
            } else {
                alertNotification('No data is recived from function "si.nazvezi.telescope.ssh_terminal".');
            }
        },
        function (error) {
            alertNotification('RPC function "Function "si.nazvezi.telescope.ssh_terminal" returned error: <b>' + JSON.stringify(error) + '</b>');
            console.log(error.error);
        }
    );
}

// rundeckCurl
function rundeckCurl(value) {
    if (empty(value)) {
        console.log("Function >> " + arguments.callee.name + " << was not called. Missing arguments!");
        return false;
    }

    $.ajax({
        url: "http://10.100.2.2:4440/api/12/job/" + this.value + "/run",
        type: 'GET',
        headers: {
            "Authorization": "Basic " + btoa("Terminator:NZteraminator"),
            "X-Rundeck-Auth-Token": "MEkINvPhEsSAuTHeDWXarlQ1qFzvpw7J",
            "Content-Type": "application/json"
        },
        crossDomain: true,
        data: {"filter": "name:" + terminali.join(',')},
        success: function (data) {
            alert(JSON.stringify(data));
        },
        error: function () {
            alert("Cannot get data");
        }
    });
}
//terminal Action
function terminalAction(session, terminali, action) {
    if(terminali.length > 0){
        //$.each(terminali, function (index, terminal) {
            //console.log(terminal, action);
        var terminal = terminali;
            switch (action) {
                case "warrning-on":
                    var args = ["C:\\tools\\scripts\\hstart.exe","/NOCONSOLE","/NOELEVATE","C:\\tools\\ahk\\AutoHotkey.exe C:\\tools\\ahk\\auser.ahk"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "warrning-off":
                    var args = ["AutoHotkey"]; // stop_proc
                    vampire_stop_proc(session, terminal, args);
                    break;
                case "middleware-reboot":
                    var args = ["shutdown","-r","-t","1"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "middleware-kill":
                    var args = ["Middleware"]; // stop_proc
                    vampire_stop_proc(session, terminal, args);
                    break;
                case "middleware-restart":
                    var args = ["Middleware"]; // stop_proc
                    vampire_stop_proc(session, terminal, args);
                    var args2 = ["C:\\tools\\scripts\\hstart.exe","/NOCONSOLE","/NOELEVATE","C:/Users/TerminalAdmin/Desktop/Terminal.Middleware.appref-ms"]; // start_proc
                    vampire_start_proc(session, terminal, args2);
                    break;
                case "middleware-start":
                    var args = ["C:\\tools\\scripts\\hstart.exe","/NOCONSOLE","/NOELEVATE","C:/Users/TerminalAdmin/Desktop/Terminal.Middleware.appref-ms"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "middleware-display":
                    var args = ["C:\\ProgramData\\AppInc\\Terminal.Middleware\\version.txt"]; // start_proc
                    console.log(vampire_start_proc(session, terminal, args));
                    break;
                case "udp-kill":
                    var args = ["udptouch.exe"]; // stop_proc
                    vampire_stop_proc(session, terminal, args);
                    break;
                case "udp-run":
                    var args = ["C:\\tools\\scripts\\hstart.exe","/NOCONSOLE","/NOELEVATE","C:\\tools\\ahk\\udptouch.exe"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "vnc-one":
                    var args = ["C:\\Windows\\SysWOW64\\reg.exe","IMPORT","C:\\tools\\scripts\\vnc-disp1.reg"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "vnc-two":
                    var args = ["C:\\Windows\\SysWOW64\\reg.exe","IMPORT","C:\\tools\\scripts\\vnc-disp2.reg"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "vnc-both":
                    var args = ["C:\\Windows\\SysWOW64\\reg.exe","IMPORT","C:\\tools\\scripts\\vnc-both.reg"]; // start_proc
                    vampire_start_proc(session, terminal, args);
                    break;
                case "wampire-kill": // stop_proc
                    var args = [];
                    var sessionOpen = session.isOpen;
                    session.call('si.nazvezi.watcher.'+ terminal +'.stop', args).then(
                        function (data) {
                            if (data != "" && sessionOpen) {
                                responseNotification('Function "si.nazvezi.watcher.' + terminal + '.start_proc" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>');
                            } else {
                                alertNotification('No data is recived from function "si.nazvezi.watcher.' + terminal + '.start_proc".');
                            }
                        },
                        function (error) {
                            alertNotification('RPC function "si.nazvezi.watcher.' + terminal + '.start_proc" returned error: <b>' + error.error + '</b>');
                            console.log(error.error);
                        }
                    );
                    break;
                case "wampire-kill-run":
                    var args = [terminal,'/cygdrive/c/tools/scripts/hstart.exe /NOCONSOLE /NOELEVATE /D="c://tools//wampire//" "c://tools//wampire//wampire.exe  -json=watcher.json '];
                    var sessionOpen = session.isOpen;
                    session.call('si.nazvezi.telescope.ssh_terminal', [terminal, 'taskkill /F /IM wampire.exe']).then(
                        function (data) {
                            if (data != "" && sessionOpen) {
                                responseNotification('Function "si.nazvezi.telescope.ssh_terminal" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>');
                                watcher_run(session,args);
                            } else {
                                alertNotification('No data is recived from function "si.nazvezi.telescope.ssh_terminal".');
                            }
                        },
                        function (error) {
                            alertNotification('RPC function "Function "si.nazvezi.telescope.ssh_terminal" returned error: <b>' + error.error + '</b>');
                            console.log(error.error);
                        }
                    );
                    break;
                case "wampire-run":
                    var args = [terminal,'/cygdrive/c/tools/scripts/hstart.exe /NOCONSOLE /NOELEVATE /D="c://tools//wampire//" "c://tools//wampire//wampire.exe  -json=watcher.json '];
                    var sessionOpen = session.isOpen;
                    watcher_run(session,args);
                    break;
                case "wampire-list":
                    var args = []; // si.nazvezi.listener.list_terminals
                    var sessionOpen = session.isOpen;
                    session.call('si.nazvezi.listener.list_terminals', args).then(
                        function (data) {
                            if (data != "" && sessionOpen) {
                                responseNotification('Function "Function "si.nazvezi.telescope.ssh_terminals" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>');
                            } else {
                                alertNotification('No data is recived from function "si.nazvezi.watcher.' + terminal + '.start_proc".');
                            }
                        },
                        function (error) {
                            alertNotification('RPC function "Function "si.nazvezi.telescope.ssh_terminals" returned error: <b>' + error.error + '</b>');
                            console.log(error.error);
                        }
                    );
                    break;
                case "wampire-update":
                    var args = []; //not working yet
                    break;
                case "wamp-maintenance":
                    console.log(terminal);
                    var sessionOpen = session.isOpen;
                    session.call("si.nazvezi.listener.send_to_watchers",[{"function":"maintanance", "args":args, "kwargs":{}}, terminal]).then(
                        function (data) {
                            console.log(data);
                            if (data != "" && sessionOpen) {
                                //return 'Function "si.nazvezi.watcher.' + terminal + '.start_proc" recived data: <blockquote>' + JSON.stringify(data) + '</blockquote>';
                                var output = "";
                                $.each(terminal, function(index, termin) {
                                    output += "Terminal " + termin + " returned: " + JSON.stringify(data[termin].output[0]) + "<br>";
                                });
                                responseNotification('Function "si.nazvezi.watcher.' + terminal + '.start_proc" recived data: <blockquote>' + output + '</blockquote>');
                            } else {
                                //return 'No data is recived from function "si.nazvezi.watcher.' + terminal + '.start_proc".';
                                alertNotification('No data is recived from function "si.nazvezi.watcher.' + terminal + '.maintanance".');
                            }
                        },
                        function (error) {
                            alertNotification('RPC function "si.nazvezi.watcher.' + terminal + '.maintanance" returned error: <b>' + error.error + '</b>');
                            console.log(error.error);
                        }
                    );
                    break;
            }
        //});

    }
}

// loginUser
function loginUser(username, password, id, session) {
    var sessionOpen = session.isOpen;
    if(sessionOpen){
        session.call('si.nazvezi.telescope.get_terminator_user', [username]).then(
            function (response) {
                if (response.password == password) {
                    console.log("logging in");
                    localStorage.setItem('login_group', response.groups);
                    localStorage.setItem('login_uid', response.topic);
                    localStorage.setItem('login_name', response.fullname);
                    localStorage.setItem('wamp_username', username);
                    localStorage.setItem('wamp_pwd', password);
                    switch(localStorage.getItem("login_group")){
                        case "admin":
                            window.location = "teleskop.html";
                            break;
                        case "support":
                            window.location = "teleskop.html";
                            break;
                        case "user":
                            window.location = "terminal_partners.html?partner="+response.topic;
                            break;
                        default:
                            localStorage.setItem('login_uid', '');
                            localStorage.setItem('login_name', '');
                            localStorage.setItem('wamp_username', '');
                            localStorage.setItem('wamp_pwd', '')
                            window.location = "index.html";
                    }
                } else {
                    alertNotification("Login was unsuccessful, please check your username and password");
                }
            },
            function (error) {
                alertNotification('RPC function "si.nazvezi.telescope.get_terminator_user" returned error: <b>' + error.error + '</b>');
                console.log(error.error);
            }
        );
    } else {
        alertNotification('No connection...');
    }
}

function check_user(session){
    //sideshow
    $("#sideshow li").each(function () {
        $( this ).css("display", "none");
    });

    switch(localStorage.getItem("login_group")){
        case "admin":
            $("#sideshow .admin ").each(function () {
                $( this ).css("display", "block");
            });
            returnPartners(session);
            console.log("v adminu");
            break;
        case "support":
            $("#sideshow .support").each(function () {
                $( this ).css("display", "block");
            });
            returnPartners(session);
            break;
        case "user":
            $("#sideshow .user").each(function () {
                $( this ).css("display", "block");
            });
            $(".filters #filterByCAPA").css("display", "none");

            var aside= '\
            <li><a href="terminal_partners.html?partner='+localStorage.getItem('login_uid')+'"><i class="fa fa-user"></i><span>All accounts</span></a></li>';
            $("#sideshow").html($("#sideshow").html() + aside);
            if(window.location.toString().indexOf("terminals.html") == -1 && window.location.toString().indexOf("terminal_partners.html?partner="+localStorage.getItem("login_uid")) == -1 ){
                window.location="terminal_partners.html?partner="+localStorage.getItem("login_uid");
            }
            break;
        default:
            localStorage.setItem('login_uid', '');
            localStorage.setItem('login_name', '');
            localStorage.setItem('wamp_username', '');
            localStorage.setItem('wamp_pwd', '');
            window.location = "index.html";
    }
    document.getElementById("sideshow").style.display = "block";
}

// user action subscription
function get_useraction_subscription(session) {
    if (session) {
        //console.log(session);
        var username = localStorage.getItem("wamp_username");
        session.subscribe("user.actions", function (data) {
            $('#subscriptionLogs').append(data[0].data.message);
            $('#subscriptionLogs').animate({scrollTop: $('#subscriptionLogs').prop("scrollHeight")}, 500);
            if ($("#subscriptionLogs:has(*)").length) {
                $('#subscriptionLogsBox').show();
            }
        });

    } else {
        console.log("not connected");
    }
}

// user action publish
function set_useraction_publish(session, user, msg, actionPress, page) {
    if (session) {
        var dt = new Date();
        var time = ("0" + dt.getDay()).slice(-2) + "." + ("0" + (dt.getMonth() + 1)).slice(-2) + "." + dt.getFullYear() + " @ " + dt.getHours() + ":" + dt.getMinutes();
        var text = '<p><span class="name text-blue"><small class="text-muted "><i class="fa fa-calendar"></i> ' + time + '</small> ' + user + '</span> clicked <span class="text-bold">' + msg + ' on ' + page + ': '+ actionPress+'</span></p>';
        session.publish("user.actions", [{
            "appname": "terminator",
            "ts": new Date(),
            "data": {
                "user": localStorage.getItem("wamp_username"),
                "message": text,
				"raw_message":user+' clicked '+msg+' on terminal: '+ actionPress
            }
        }]);
        $('#subscriptionLogs').append(text);
        $('#subscriptionLogs').animate({scrollTop: $('#subscriptionLogs').prop("scrollHeight")}, 500);
    } else {
        console.log("not connected");
    }
}

// screenshots subscription
function get_screenshots_subscription(session) {
    if (session) {
        session.subscribe("screenshots", function (data) {
            var computer_name = data[0].data.computer_name;
            var latest = data[0].data.latest;
            var imgtime = new Date();
            setTimeout(function () {
                $('#img' + computer_name).attr("src", latest + '?' + imgtime.getTime());
            }, 1000);
        });
    } else {
        console.log("not connected");
    }
}

// logs subscription
// for update push
function get_logs_subscription(session) {
    if (session) {
        session.subscribe("filter", function (data) {
            var term_id = data[0].data.id;
            var term_log = data[0].data.log;
            //console.log(data);
            $('#' + term_id + 'log').attr('data-original-title', term_log);
            var count = $('#' + term_id + 'log').attr('data-log-count');
            if (count < 4) {
               $('#' + term_id + 'log').attr('data-log-count', count++);
            }
            //dodano
            $('#terminalsLogs').append(term_log+'</br>');
            $('#terminalsLogs').animate({scrollTop: $('#terminalsLogs').prop("scrollHeight")}, 500);
            if ($("#terminalsLogs:has(*)").length) {
                $('#terminalsLogsBox').show();
            }
        });
    } else {
        console.log("not connected");
    }
}

// log subscription for core services
function get_log_subscrip_core(session) {
    if (session) {
        session.subscribe("core_services_status", function (data) {
            var term_id = data[0].data;
            //console.log(data);
            var d = new Date();
            var datestring = d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " +
                d.getHours() + ":" + d.getMinutes();
            if(term_id != null){
                if(term_id.status == false){
                    $('#logs2').append('['+datestring +']'+term_id.service+' is offline</br>');
                }else{
                    $('#logs2').append('['+datestring +']'+ term_id.service+' is online</br>');
                }

                $('#logs2').animate({scrollTop: $('#logs2').prop("scrollHeight")}, 500);
            }

        });
    } else {
        console.log("not connected");
    }
}

// log subscription
function get_log_subscription(session, terminal) {
    if (session) {
        session.subscribe("logs", function (data) {
            var term_id = data[0].data.id;
            var term_log = data[0].data.log;
            if(terminal == term_id){
                $('#logs').append(data);
                $('#logs').animate({scrollTop: $('#logs').prop("scrollHeight")}, 500);
            }
        });
    } else {
        console.log("not connected");
    }
}
//coreservice button subscription
function get_subscription_button(session,data){
    console.log(data);
    if (session) {
        console.log(typeof data);
        session.subscribe(data, function (response) {
            var text = JSON.stringify(response[0].data)+"<br>";
            $('#'+data+'_pubsub pre').append(text);
        }).then(
            function(subscription) {
                $('#'+data+'_pubsub pre').val(subscription);
            },
            function(error){
                console.log("SUBSCRIBE ERROR FOR", data, ", ERROR:", error);
            }
        );
    } else {
        console.log("not connected");
    }

}

function get_computers_data(session, data) {
    //console.log(data);
    if (data) {
        var sorted_data = data.sort(function (a, b) {
            var x = a.CAPA.toLowerCase();
            var y = b.CAPA.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        sorted_data = sorted_data.sort(function (a, b) {
            var x = a.CAPA.toLowerCase();
            var y = b.CAPA.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        var display = '';
        var sorted = {};
        $.each(sorted_data, function (key, value) {
            sorted[key] = value;
        });

        $.each(sorted, function (key, value) {
            var computer_name = value.computer_name;
            var capa = value.CAPA;
            var in_state = value.in_state;
            var ext_ip = value.ext_ip;
            var ip_address = value.ip_address;
            var last_seen = value.last_seen;
            var serial_number = value.serial_number;
            var prod_confirmed = value.prod_confirmed;
            var vnc_monitor = value.allow_vnc_monitor;
            //console.log(last_seen);

            var color = "";
            switch (in_state) {
                case "test":
                    color = "warning";
                    break;
                case "prod-test":
                    color = "info";
                    break;
                case "prod":
                    color = "danger";
                    break
            }

            display += '<div id="' + computer_name + '" style="display:none;" class="col-lg-3 col-md-6 terminal" name="terminals"data-putProduction="false" data-shouldwork="false" data-working="false" data-notworking="false" data-all="true" data-search-serial="' + serial_number + '" data-search-ip="' + ip_address + '" data-search-name="' + computer_name + '" data-search-partner="'+ capa + '">';
            display += '    <div class="box">';
            display += '        <div class="box-header with-border">';

            var terminal_info = "Name: " + computer_name + '<br> State: ' + in_state + '<br> Partner: ' + capa + '<br> External IP: ' + ext_ip + '<br> Last seen: ' + last_seen + '<br> Serial: ' + serial_number + '<br> Prod confirmed: ' + prod_confirmed + '<br> VNC monitor: ' + vnc_monitor;

            display += '            <h3 class="box-title"><i class="fa fa-tv" data-toggle="tooltip" data-html="true" data-original-title="' + terminal_info + '"></i> ' + capa + '</h3>';
            display += '            <span class="label label-info"></span>';
            display += '            <div class="box-tools pull-right">';
            display += '                <a href="https://zabbix.egaming.systems/search.php?search=' + computer_name + '" target="_blank" class="actionButton" data-action="Zabbix link"><img src="dist/img/zabbix.png" height="30" /></a>';
            display += '                <a href="vnc:' + ip_address + '" target="_blank" class="actionButton" data-action="VNC connection" data-tid="' + computer_name + '"><img src="dist/img/vnc.png" height="30" /></a>';
            display += '                <span class="btn btn-sm btn-' + color + '">' + in_state + '</span>';
            display += '            </div>';
            display += '        </div>';
            display += '        <div class="box-body">';

            var image = 'dist/img/' + capa + '_.jpg';

            display += '            <span id="' + computer_name + 'log" data-toggle="tooltip" data-html="true" data-log-count="1" data-original-title=""><img class="img-responsive pad" width="100%" id="img' + computer_name + '" src="' + image + '" title="' + computer_name + '"></span>';
            display += '        </div>';
            display += '        <div class="box-footer with-border">';
            display += '            <div class="input-group">';

            if(value.status.status == "pending"){
                display += '                <a href="terminal_accounts.html?status=pending&tid='+ computer_name+'" target="_blank" class="actionButton label label-primary" data-action="details">Details Link</a>';
            }else{
                display += '                <a href="terminaldetail.html?tid=' + computer_name + '" target="_blank" title="Detail view for ' + computer_name + '" class="btn btn-primary"><i class="fa fa-search-plus"></i> Detail view</a>';
            }
            display += '                <span class="input-group-addon">' + computer_name + '</span>';
            display += '                <span class="input-group-addon icheckbox"><input type="checkbox" class="' + capa + '-' + in_state + '" value="' + computer_name + '" /></span>';
            display += '            </div>';
            display += '        </div>';
            display += '    </div>';
            display += '</div>';
        });
        $('#clients').html(display);

// && (value.status == "pending" || value.status == "in_prod")
        $.each(sorted, function (key, value) {
            if(value.prod_confirmed != true && (value.in_state == "prod" || value.in_state == "prod-test")) {
                $('#' + value.computer_name).attr('data-putProduction', 'true');
                console.log(value);
            }
            if(value.status.status == "pending"){
                $('#' + value.computer_name).attr('data-all', 'false');
            }
            // get computer status
            session.call('si.nazvezi.watcher.' + value.computer_name + '.ping').then(
                function (result) {
                    //console.log(value);
                    if (value.allow_vnc_monitor) {
                        $('#' + value.computer_name).attr('data-working', 'true');
                        $('#' + value.computer_name).attr('data-shouldwork', 'true');
                        $('#' + value.computer_name + ' .box').addClass('box-success');
                        $('#' + value.computer_name + '').css('display', 'block');
                    } else {
                        $('#' + value.computer_name).attr('data-working', 'true');
                        $('#' + value.computer_name + ' .box').addClass('box-warning');
                    }
					session.call("wamp.registration.match", ['si.nazvezi.watcher.' + value.computer_name + '.ping']).then(
                        function(response){
                            //console.log(response);
                            $('#' + value.computer_name + ' .box').addClass(response.toString());
                        },
                        function(error){
                            console.log(error);
                        }
                    );
                },
                function (error) {
                    if (value.allow_vnc_monitor) {
                        $('#' + value.computer_name).attr('data-notworking', 'true');
                        $('#' + value.computer_name).attr('data-shouldwork', 'true');
                        $('#' + value.computer_name + ' .box').addClass('box-danger');
                        $('#' + value.computer_name + '').css('display', 'block');
                    } else {
                        $('#' + value.computer_name + ' .box').addClass('box-default');
                    }
                }
            );
            // get computer vnc connections
            session.call('si.nazvezi.watcher.' + value.computer_name + '.get_vnc_connections').then(
                function (result) {
                    if (result.count > 0) {
                        $('#' + value.computer_name + ' .box .label').html('Active VNC connections: ' + result.count);
                    }
                },
                function (error) {
                    $('#' + value.computer_name + ' .box .label').html();
                }
            );
        });
    } else {
        alertNotification('No data is recived from function "si.nazvezi.telescope.get_computers_data".');
    }
}

function get_computer_data(session, data) {
    var wampire_res ="";
    var computer_name = data.computer_name;
    //console.log("Tuki v normalnemu computer_name");
    session.call('si.nazvezi.watcher.' + computer_name + '.version').then(
        function (response) {
            wampire_res=response.args[0];
            console.log(response);
            get_computer_data_with_version(session,data,wampire_res);
        },
        function (error) {
            wampire_res="offline";
            console.log("error");
            get_computer_data_with_version(session,data,wampire_res);
        }
    );
}

function get_computer_data_with_version(session, data,wampire_res) {
    //console.log("Notr v get computer data", data);
    if (data) {
        console.log(data);
        var partner_name = data.partner_name;
        var computer_name = data.computer_name;
        var capa = data.CAPA;
        var in_state = data.in_state;
        var ext_ip = data.ext_ip;
        var ip_address = data.ip_address;
        var last_seen = data.last_seen;
        var serial_number = data.serial_number;
        var prod_flag = data.prod_flag;
        var prod_confirmed = data.prod_confirmed;
        var vnc_monitor = data.allow_vnc_monitor;

        containor_status(session,computer_name);
        var display = '';
        var color = "";
        switch (in_state) {
            case "test":
                color = "warning";
                break;
            case "prod-test":
                color = "info";
                break;
            case "prod":
                color = "danger";
                break;
            default:
                color = "default";
                break;
        }
        display += '<div id="' + computer_name + '" style="display:none;" class="col-lg-12 col-md-12 terminal" name="terminals" data-shouldwork="false" data-working="false" data-notworking="false" data-all="true" data-search-serial="' + serial_number + '" data-search-ip="' + ip_address + '" data-search-name="' + computer_name + '">';
        display += '    <div class="box">';
        display += '        <div class="box-header with-border">';
        display += '            <h3 class="box-title"><i class="fa fa-tv"></i> ' + capa + '</h3>';
        display += '            <span class="label label-info"></span>';
        display += '            <div class="box-tools pull-right">';
        display += '                <div class="btn-group">';

        display += '                    <div class="col-sm-12">';
        display += '                    <select id="select_action_details" class="form-control" placeholder="Select action" style="width:70%; display: inline">';
        display += '                        <option value="Select" > Select action</option>';
        display += '                        <optgroup label="Ausser">';
        display += '                        <option value="warrning-on">Ausser ON</option>';
        display += '                        <option value="warrning-off">Ausser OFF</option>';
        display += '                    </optgroup>';
        display += '                    <optgroup label="Middleware">';
        display += '                        <option value="middleware-reboot">Reboot terminal</option>';
        display += '                        <option value="middleware-kill">Kill Terminal Middleware</option>';
        display += '                        <option value="middleware-restart">Restart Terminal Middleware</option>';
        display += '                        <option value="middleware-start">Start Terminal Middleware</option>';
        display += '                        <option value="middleware-display">Display Middleware version</option>';
        display += '                    </optgroup>';
        display += '                    <optgroup label="UDPTouch">';
        display += '                        <option value="udp-kill">Kill UDPTouch.exe</option>';
        display += '                        <option value="udp-run">Run UDPTouch.exe</option>';
        display += '                        </optgroup>';
        display += '                        <optgroup label="VNC">';
        display += '                       <option value="vnc-one">Use screen 1 for VNC</option>';
        display += '                       <option value="vnc-two">Use screen 2 for VNC</option> ';
        display += '                       <option value="vnc-both">Use screen 1&2 for VNC</option>';
        display += '                    </optgroup>';
        display += '                    <optgroup label="Wampire">';
        display += '                        <option value="wampire-kill">Kill Wampire.exe</option>';
        display += '                        <option value="wampire-run">Run Wampire.exe</option>';
        display += '                        <option value="wampire-kill-run">Kill & Run Wampire.exe</option>';
        display += '                        <option value="wamp-maintenance">Wampire maintenance</option>';
        display += '                        </optgroup>';
        display += '                    </select>';
        display += '                        <button type="button" class="btn btn-primary actionButton" id="actionExecute" style="width:28%;margin: 0px; float: right">Execute</button>';
        display += '                    </div>';
        display += '                </div>';
        display += '            </div>';
        display += '        </div>';
        display += '        <div class="box-body">';
        display += '            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">';
        display += '                <div class="box-body table-responsive no-padding">';
        // adding vampire on / off
        display += '                    <div><p id="vampire_verz"><b>Wampire : </b>'+ wampire_res+'</p>';
        display += '                    </div>';
        display += '                    <table class="table table-hover">';
        display += '                        <thead>';
        display += '                            <tr><th>Data</th><th>Value</th></tr>';
        display += '                        </thead>';
        display += '                        <tbody>';
        display += '                            <tr><td>Name</td><td>'+ computer_name +'</td></tr>';
        display += '                            <tr><td>Details</td><td><a href="terminal_accounts.html?status=in_production&tid='+ computer_name+'" target="_blank" class="actionButton label label-primary" data-action="details" data-toggle="tooltip" data-original-title="Click to go in details">Link to details</a></td></tr>';
        var balance = "Click to get balance";
        display += '                            <tr><td>Balance</td><td><a id="check_button" target="_blank" class="label label-primary" data-action="logs" data-toggle="tooltip" data-original-title="Click to get balance">'+balance+'</a></td></tr>';
        display += '                            <tr><td>State</td><td><span class="label label-' + color + '">'+ in_state +'</span></td></tr>';
        display += '                            <tr><td>Partner</td><td>'+ capa +'</td></tr>';
        display += '                            <tr><td>External IP</td><td>'+ ext_ip +'</td></tr>';
        display += '                            <tr><td>Last seen</td><td>' + last_seen +'</td></tr>';
        display += '                            <tr><td>Serial</td><td>'+ serial_number +'</td></tr>';
        display += '                            <tr><td>Prod confirmed</td><td>'+ ((prod_confirmed) ? '<span class="label label-success">'+ prod_confirmed+'</span>' : '<span class="label label-danger">'+ prod_confirmed+'</span>') +'</td></tr>';
        display += '                            <tr><td>VNC monitor</td><td>'+ ((vnc_monitor) ? '<span class="label label-success">'+ vnc_monitor+'</span>' : '<span class="label label-danger">'+ vnc_monitor+'</span>') +'</td></tr>';
        display += '                            <tr><td>Active VNC connections</td><td><span id="vnc_conn" class="label label-info"></span></td></tr>';
        display += '                            <tr><td>Zabbix</td><td><a href="https://zabbix.egaming.systems/search.php?search=' + computer_name + '" target="_blank" class="actionButton label label-primary" data-action="Zabbix link" data-toggle="tooltip" data-original-title="Click to go in Zabbix">zabbix.egaming.systems</a></td></tr>';
        display += '                            <tr><td>VNC</td><td><a href="vnc:' + ip_address + '" target="_blank" class="actionButton label label-primary" data-action="VNC connection" data-toggle="tooltip" data-original-title="Connect to VNC">' + ip_address + '</a></td></tr>';
        display += '                            <tr><td>Established VNC</td><td><span id="vnc_out"></span></td></tr>';
        display += '                            <tr><td>Videos</td><td><a href="'+ video_link + date_folder + '/'+capa+'/' + computer_name +'" target="_blank" class="label label-primary" data-action="Videos folder">Videos '+ computer_name +'</a></td></tr>';
        display += '                            <tr><td>Logs</td><td><a href="https://terminator.silkretail.com:4555/Video/logsfromterminals/'+capa+'/'+computer_name+'/" target="_blank" class="label label-primary" data-action="logs">Logs '+ computer_name+'</a></td></tr>';
        display += '                            <tr><td>Sync logs</td><td><a id="sync_button" target="_blank" class="label label-primary" data-action="logs">Sync logs from '+computer_name+'</a></td></tr>';
        display += '                            <tr><td>Container</td><td><a id="container-status" class="label label-primary" >Kill recording client</a></td></tr>';

        display += '                        </tbody>';
        display += '                    </table>';
        display += '                </div>';
        display += '            </div>';

        display += '            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right" id="newscreenshoot">'
        display += '                <span id="' + computer_name + 'log" data-toggle="tooltip" data-html="true" data-log-count="1" data-original-title=""><img class="img-thumbnail img-responsive pad center-block" width="100%" id="img" src="dist/img/loading.gif" title="' + computer_name + '"></span>';
        display += '            </div>';
        display += '        </div>';
        display += '        <div class="box-footer with-border">';
        display += '            <div class="btn-group">';
        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Make Computer Vision screenshoot of terminal ' + computer_name + '" data-action="Make CV screenshoot" style="margin-left: 3px;"><i class="fa fa-photo"></i> Make CV screenshoot</button>';
        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Copy VNC to clipboard" onclick="copyToClipboard(&quot;vnc:' + ip_address + '&quot)" data-action="Copy VNC" style="margin-left: 3px;"><i class="fa fa-clipboard"></i> Copy VNC</button>';
        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Generate SSH and copy to clipboard" data-action="Copy SSH" onclick="copyToClipboard(\'ssh TerminalAdmin@' + ip_address + ' -i  ~/.ssh/win7embeded.ossh -o ConnectTimeout=1 -o ConnectionAttempts=1 -o StrictHostKeyChecking=no\')"style="margin-left: 3px;" ><i class="fa fa-clipboard"></i> Copy SSH</button>';

        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Container SSH and copy to clipboard" data-action="Copy cont SSH" onclick="copyToClipboard()" style="margin-left: 3px;"><i class="fa fa-clipboard"></i> Copy Container SSH</button>';
        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Link to container log" data-action="Cont log" onclick="copyToClipboard()" style="margin-left: 3px; "><i class="fa fa-location-arrow"></i> Container log</button>';
        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Link to container VNC" data-action="Cont VNC" onclick="copyToClipboard()" style="margin-left: 3px; "><i class="fa fa-location-arrow"></i> Container VNC</button>';


        display += '            </div>';
        display += '            <div class="btn-group pull-right">';
        display += '                <button class="btn btn-sm btn-danger actionButton1" title="Start recording terminal" data-action="Start recording"><i class="fa fa-video-camera"></i> Start recording</button>';
        display += '                <button class="btn btn-sm btn-primary actionButton1" title="Stop recording terminal" data-action="Stop recording"><i class="fa fa-stop"></i> Stop recording</button>';
        display += '            </div>';
        display += '        </div>';
        display += '    </div>';
        display += '</div>';

        $('#client').html(display);

        //get a new screenshoot on click
        $('#newscreenshoot').on('click', function() {
            var  computer= [url_param("tid")];
            session.call('si.nazvezi.watcher.' + computer+ '.screenshot').then(
                function (response) {
                    $('#img').attr("src",'data:image/jpg;base64,' + response);

                },
                function(error){
                    console.log(error);
                }
            );

        });

        $('#actionExecute').on('click', function() {
            var val_action = $('#select_action_details').val();
            console.log(val_action);
            var  terminali= [url_param("tid")];
            terminalAction(session, terminali, val_action);
            //rpccallSelecTermi(terminali, val_action, session);
        });

        $('#container-status').on('click', function() {
            containor_kill(session, computer_name);
        });
        $('#sync_button').on('click', function() {
            responseNotification("Started synchronizing, please wait.");
            session.call('si.nazvezi.watcher.' + computer_name + '.rsync', ["C:\\tools\\cygwin64\\bin\\rsync -vr /cygdrive/c/ProgramData/AppInc/Terminal.Middleware/ rsync://10.100.0.10/term_logs/"+capa+"/"+computer_name+"/"]).then(
                function (result) {
                    responseNotificationOK('Sync logs on terminal '+computer_name+' is completed.');
                },
                function (error) {
                    alertNotification('RPC function "si.nazvezi.listener.user_actions" returned error: <b>'+error.error+'</b>');
                    console.log(error.error);
                }
            );
        });

        // get computer status
        session.call('si.nazvezi.watcher.' + computer_name + '.ping').then(
            function (result) {
                if (vnc_monitor) {
                    $('#' + computer_name).attr('data-working', 'true');
                    $('#' + computer_name).attr('data-shouldwork', 'true');
                    $('#' + computer_name + ' .box').addClass('box-success');
                    $('#' + computer_name + '').css('display', 'block');
                } else {
                    $('#' + computer_name).attr('data-working', 'true');
                    $('#' + computer_name + ' .box').addClass('box-warning');
                    $('#' + computer_name + '').css('display', 'block');
                }
				session.call("wamp.registration.match", ['si.nazvezi.watcher.' + computer_name + '.ping']).then(
                    function(response){
                        console.log(response);
                        $('#' + computer_name + ' .box').addClass(response.toString());
                    },
                    function(error){
                        console.log(error);
                    }
                );
            },
            function (error) {
                if (vnc_monitor) {
                    $('#' + computer_name).attr('data-notworking', 'true');
                    $('#' + computer_name).attr('data-shouldwork', 'true');
                    $('#' + computer_name + ' .box').addClass('box-danger');
                    $('#' + computer_name + '').css('display', 'block');
                } else {
                    $('#' + computer_name + ' .box').addClass('box-default');
                }
            }
        );
        // get computer vnc connections
        session.call('si.nazvezi.watcher.' + computer_name + '.get_vnc_connections').then(
            function (result) {

                if (result.count > 0) {
                    $('#vnc_conn').html(result.count);

                    var conn = [];
                    $.each(result.out, function (key, value) {
                        conn[key] = value.split("     ");
                    });
                    $.each(conn, function (key, value) {
                        $('#vnc_out').append(value[1]+'<br>');
                    });
                }
            },
            function (error) {
                $('#vnc_conn').html(0);
            }
        );
    } else {
        alertNotification('No data is recived from function "si.nazvezi.telescope.get_computer_data".');
    }
}
function containor_kill(session, computer_name){
    var sessionOpen = session.isOpen;
    session.call('si.nazvezi.cbs.kill_recording_client',[computer_name]).then(
        function (result) {
            console.log("Killed recording client");
        },
        function (error) {
            console.log(error);
        }
    );
}
function containor_status(session, computer_name){
    console.log(computer_name);
    var sessionOpen = session.isOpen;
    session.call('si.nazvezi.cbs.get_terminal_details',[computer_name]).then(
        function (result) {
            if (result != "Terminal is not initialized") {
                console.log(result);
                $('#con1').html('<pre>'+ result.TASK_ID + '</pre>');
                $('#con2').html('<pre>'+ result.connect + '</pre>');
            }else{
                console.log(result);
            }
        },
        function (error) {
            console.log(error);
        }
    );
}

function CBSdown() {
    var content = $(".content");
    content.block({
        message: "Please wait ..."
    });
    $.ajax({
        url: "http://192.168.207.91/service/marathon-master/v2/apps/nadzornik/crossbario",
        type: "PUT",
        data: {
            instances: 0
        },
        success: function () {
            content.unblock({
                onUnblock: function () {
                    alert("CBS is shooting down");
                }
            });
        },
        error: function () {
            content.unblock({
                onUnblock: function () {
                    alert("Something is wrong");
                }
            });
        }
    });
}

function CBSup() {
    var content = $(".content");
    content.block({
        message: "Please wait ..."
    });
    $.ajax({
        url: "http://192.168.207.91/service/marathon-master/v2/apps/nadzornik/crossbario",
        type: "PUT",

        data: {
            instances: 1
        },
        success: function () {
            content.unblock({
                onUnblock: function () {
                    alert("CBS is starting now");
                }
            });
        },
        error: function () {
            content.unblock({
                onUnblock: function () {
                    alert("Something is wrong");
                }
            });
        }
    });
}

// function for partner and terminal accounts
function returnPartners(session){
    //console.log(session);
    session.call('si.nazvezi.telescope.get_partners_info').then(
        function (response) {
            var tmpArray = [];
            $.each(response, function (key, response_data) {
                tmpArray.push(response_data.partner_country+response_data.partner_name);
            });

            tmpArray.sort();
            var display = '';
            $.each(tmpArray, function (key, partners) {

                document.getElementById("partner_menu").innerHTML +='<li><a href="terminal_partners.html?partner='+partners+'">'+partners+'</a></li>';
            });
            //document.getElementById("account_menu").innerHTML =display;
            document.getElementById("account_menu").innerHTML +='<li><a href="terminal_accounts.html?status=pending"> Pending</a></li>';
            //document.getElementById("account_menu").innerHTML +='<li><a href="terminal_accounts.html?status=created_acc"> Created</a></li>';
            document.getElementById("account_menu").innerHTML +='<li><a href="terminal_accounts.html?status=in_production">In production</a></li>';
        });

}

// ...
function returnPartnersForSelect(session){
    session.call('si.nazvezi.telescope.get_partners_info').then(
        function (response) {
            var tmpArray = [];
            $.each(response, function (key, response_data) {
                tmpArray.push(response_data.partner_country+response_data.partner_name);
            });

            tmpArray.sort();
            var display = '';
            display += '<option selected = "selected"> -All-</option>';
            $.each(tmpArray, function (key, partners) {
                display +='<option>'+partners +'</option>';
                //document.getElementById("partner_menu").innerHTML +='<li><a href="terminal_partners.html?partner='+partners+'">'+partners+'</a></li>';
            });
            document.getElementById("search_partners1").innerHTML = display;
        });

}

//function for display all terminal accounts
function get_acc_data(session,data) {
    localSession=session;
    if (data !=null) {
        var display = '';

        var sorted_data = data.sort(function (a, b) {
            var x = a.id;
            var y = b.id;
            return x > y ? -1 : x < y ? 1 : 0;
        });

        $.each(data, function (key, response_data) {

            var color = "";
            switch (response_data.status.status) {
                case "created_acc":
                    color = "danger";
                    break;
                case "in_production":
                    color = "info";
                    break;
                case "pending":
                    color = "warning";
                    break
            }

            display += '<div id="' + response_data.computer_name + '" style="display:block; margin-left: 16px; margin-right: 16px; min-width: 200px; max-width: 300px " class="col-lg-3 col-md-6 terminal" name="terminals" data-search-name="' + response_data.computer_name + '" data-search-serial="'+response_data.serial_number+'" >';
            display += '    <div class="box box-' + color + '  collapsed-box">';
            display +='       <button type="button" class="btn btn-box-tool class=col-lg-12" data-widget="collapse" style="width: 100%;">';
            display += '        <div class="box-header with-border" >';
            display += '            <h3 class="box-title" style="float: left"><i class="fa fa-tv" data-toggle="tooltip" data-html="true"></i> ' + response_data.computer_name + '</h3>';
            display += '        </div>';
            display += '        </button>';
            display += '        <div class="box-body">';
            // kar se bo pokazalo za pluskom
            display += '            <form>';
            display += '                <div class="form-group">';

            display += '                        <label>Terminal name: <span style="font-weight: normal">'+response_data.computer_name +'</span></label>';
            display += '                        <label>Terminal admin:</label>';
            display += '                    <ul class="form-group">';
            display += '                        <li class="form-group">';
            display += '                            <p><b>Username:</b> '+response_data.mw_un+'</p>'
            display += '                        </li>';
            display += '                        <li class="form-group">';
            display += '                            <p><b>Password: </b>'+response_data.mw_pw+'</p>'
            display += '                        </li>';
            display += '                        <li class="form-group">';
            display += '                            <button type="button" class="btn btn-primary label" id="'+response_data.computer_name+'.cheak_barcode" onclick="get_barcode('+response_data.computer_name+')">Print barcode</button>';
            display += '                        </li>';

            display += '                    </ul>';

            display += '                    <label>Cashdesk:</label>';
            display += '                    <ul class="form-group">';
            display += '                        <li class="form-group"></label><p>';
            display += '                            <p><b>Username:</b> '+response_data.cd_un+'</p>';
            display += '                        </li>';
            display += '                        <li class="form-group">';
            display += '                            <p><b>Password: </b>'+response_data.cd_pw+'</p>';
            display += '                        </li>';
            display += '                        <li class="form-group">';
            display += '                           <p><b>Oryx link: </b>'+response_data.oryx_admin_api+'</p>';
            display += '                        </li>';
            display += '                    </ul>';
            display += '                    <label>Terminal:</label>';
            display += '                    <ul class="form-group">';
            display += '                        <li class="form-group">';
            display += '                            <p><b>Terminal type:</b> '+response_data.terminal_type+'</p>';
            display += '                        </li>';
            display += '                        <li class="form-group">';
            display += '                        <p><b>Serial number: </b>'+response_data.serial_number+'</p>';
            display += '                        </li>';
            display += '                        <li class="form-group">';
            var date= response_data.serial_created_tz.split('T');
            display += '                        <p><b>Date created: </b>'+(date[0])+'</p>';
            display += '                        </li>';
            display += '                    </ul>';
//console.log(response_data);
            display += '                    <label>Partner:</label>';
            display += '                    <ul class="form-group">';
            display += '                        <li>';
            display += '                            <p><b>Shop name: </b>'+response_data.faze.sub_partner+" "+response_data.faze.shop_name+'</p>';
            display += '                        </li>';

            display += '                        <li>';
            display += '                            <p><b>Address: </b>'+response_data.location_address+'</p>';
            display += '                        </li>';
            display += '                    </ul>';
            display += '                    <div class="form-group">';
            display += '                        <label>Can account be used in production:&nbsp;';
            //display += '                        <div class="col-sm-4">';
            check = "";
            if(response_data.prod_flag) {
                check = "checked";
            }
            display += '                           <input type="checkbox" disabled id="'+response_data.computer_name+'.prod_flag" '+ check + '></label>';
            //display += '                           </div>';
            display += '                          </div>';

            //display += '                    <div class="form-group">';
            //display += '                        <label>Status:</label><p> '+response_data.status.status+'</p>';
            //display += '                    </div>';
            display += '                </div>';
            display += '            <form>';
            //-------------------------footer linkan na skripto Account functons?

            //display += '                <button type="submit" class="btn btn-primary">Edit</button>';
            //var znacka="";
            //if (response_data.prod_flag === true){
             //   znacka="disabled";
            //}
            //display += '                <button type="submit" class="btn btn-danger '+znacka+'" >Delete</button>';
            display += '            </div>';
            display += '        </div>';
            display += '    </div>';
            display += '</div>';
        });
        //console.log("Konec");
        document.getElementById("terminal_acc").innerHTML = display;
    }else {
        alertNotification('No data is recived from function "si.nazvezi.telescope.get_partner_accounts".');
    }
}

//function for creating specific fields
function get_spec_field(session,data) {
    if (data !=null) {
        var display = '';
        var selects=[];
        $.each(data, function (key, response_data) {
            if(response_data.type == "select"){
                display += '<div class="form-group">';
                display += '    <label for="inputs'+key+'" class="col-sm-2 control-label">'+response_data.value+': </label>';
                display += '    <div class="col-sm-4">';
                display += '        <select name="inputs" data-spec="spec_inputs" class="form-control checkchar" id="'+response_data.value+'" placeholder="'+response_data.desc+'" >';
                display += '  <option></option>';
                $.each(response_data.values, function (key, element) {
                    display += '  <option value='+element+'>'+key+'</option>';
                });
                display += '         </select>';

                selects.push(response_data.value);

                display += '    </div>';
                display += '</div>';
            }else{
                display += '<div class="form-group">';
                display += '    <label for="inputs'+key+'" class="col-sm-2 control-label">'+response_data.value+': </label>';
                display += '    <div class="col-sm-4">';
                display += '        <input name="inputs" data-spec="spec_inputs" class="form-control checkchar" id="'+response_data.value+'" placeholder="'+response_data.desc+'" type="'+response_data.type+'">';
                display += '    </div>';
                display += '</div>';
            }
        });
        document.getElementById("spec_feld").innerHTML = display;
        generate_sele(selects, 1);
    }else {
        alertNotification('No data is recived from function "si.nazvezi.telescope.get_partner_accounts".');
    }
}

function generate_sele(x,y){
    $.each(x, function(key, value){
        var id = value.replace(" ", "\\ ");
        $("#" + id).select2({
            placeholder: "Select printer",
            width: "100%",
            tags: true,
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
    });
}
//cheak if name alredy exist
function cheakterminal_name(session,data,tip,check,yes,no){
    terminal_name_exist=false;
    if (data !=null) {
        //console.log(data);
        if(data.length == check || check == 0){
            //console.log(data,tip);
            session.call('si.nazvezi.telescope.check_account_name', [data, tip]).then(
                function (response) {
                    if(response != null){
                        no(response);
                    }else{
                        yes(response);
                    }
                });
        }else {
            alertNotification('Invalid terminal name! Terminal name must be '+check+' charaters long.');
        }

    }
}

function generate_name(session, data, id){
    var text = "UT";
    var array= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
    var name= data[2];
    name=name.toUpperCase();
    var name1= name.split('');
    //console.log(name1);

    var st;
    if(name1.length == 0){
        alertNotification("Unable to generate new account name. Please fill in shop name and shop address");
        return "";
    }
    for(var i=0; i<7; i++){
        st = Math.floor((Math.random() *name.length));
        //console.log(st);
        var found = false;
        for(var j=0; j<array.length; j++){
            if(name1[st] == array[j]){
                text += array[j];
                found = true;
            }
        }
        if(!found){
            i--;
        }
    }
    document.getElementById(id).value = text;
    session.call('si.nazvezi.telescope.get_suggested_name', [data[0], data[1]]).then(
        function (response) {
            document.getElementById(id).value += response;
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.telescope.get_suggested_name" returned error: <b>'+error.error+'</b>');
            console.log(error.error);
        });
}

function suggest_name(session, data, id){
    var text = "UT";
    var array= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
    var name= data[2];
    name=name.toUpperCase();
    var name1= name.split('');
    //console.log(data);

    var st;
    if(name1.length == 0){
        alertNotification("Unable to generate new account name. Please fill in shop name and shop address");
        return "";
    }
    if(name1.length >=7){
        for(var i=0; i<7; i++){
            for(var j=0; j<array.length; j++){
                if(name1[i] == array[j]){
                    text += array[j];
                    found = true;
                }
            }
            if(!found){
                i--;
            }
        }
        document.getElementById(id).value = text;
        console.log(data);
        session.call('si.nazvezi.telescope.get_suggested_name', [data[0], data[1]]).then(
            function (response) {
                document.getElementById(id).value += response;
                console.log(response);
            },
            function (error) {
                alertNotification('RPC function "si.nazvezi.telescope.get_suggested_name" returned error: <b>'+error.error+'</b>');
                console.log(error.error);
            });
    }

}

function responseNotificationOK(notification) {
    var alert = '';
    alert += '<div class="alert alert-success alert-dismissible slideup-alert fade in">';
    alert += '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    alert += '  <h4><i class="icon fa fa-check"></i> Success!</h4>';
    alert += notification;
    alert += '</div>';
    document.getElementById("responseNotification").scrollIntoView();
    $('#alertNotification').html('');
    $('#responseNotification').html(alert);
}

/*
 session.call('si.nazvezi.allpartners).then(
    function (response) {
     var color = "";
     switch (response.status) {
     case "online":
     color = "success";
     break;
     case "offline":
     color = "danger";
     break;
     case "away":
     color = "warning";
     break
     }

    var display='';
     for(var i =0; i<response.length; i++){
        display+= '<tr><td>+response.partner+</td><td><svg height="10" width="10"><circle cx="6" cy="6" r="3" fill="'+ color +'" /></svg></td></tr>';
     }
    document.getElementById("#online_partner").innerHTML = display;
    },
    function (error) {
    alertNotification('RPC function "si.nazvezi.allpartners" returned error: <b>'+error.error+'</b>');
    console.log(error.error);
 });

 */
