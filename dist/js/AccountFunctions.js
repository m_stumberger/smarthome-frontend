/**
 * Created by a.zamljen on 10.7.2017.
 */
//functoin for display all terminals by status get_acc_data_by_status

var partners_specific = {};
var terminal_ime="";

function filterTerminals() {
    var partner = document.getElementById("search_partners1").value;
    var time = document.getElementById("time_added").value;
    var stat = document.getElementById("prod_status").value;
    var search = document.getElementById("search_value2").value.toUpperCase();
    //console.log(partner+time+stat+search);
    var searchVal = search;
    var terminali = document.getElementsByName("terminals");
    if(search.length > 0){
        var searchVal = search.replace(/ /g , "");
        $.each(terminali, function(key, element){
            element.style.display = 'block';
            if ((element.getAttribute("data-search-name").indexOf(searchVal) == -1) && (element.getAttribute("data-search-serial").indexOf(searchVal) == -1)) {
                element.style.display = 'none';
            }
        });
    }
    else{
        $.each(terminali, function(key, element){
            element.style.display = 'block';
            if(element.getAttribute("data-search-partner") != partner && partner != "-All-"){
                element.style.display = 'none';
            }
            if(element.getAttribute("data-search-state") != stat && stat != "all"){
                element.style.display = 'none';
            }
            var time1 = element.getAttribute("data-search-time");
            var time2 = (Math.round(new Date(time1).getTime()/1000));
            var now = Math.round(new Date().getTime()/1000);
            if((now - time2) > parseInt(time)  && time != "all"){
                element.style.display = 'none';
            }
        });
    }

}

function get_acc_data_by_status(session,data) {
    localSession = session;
    if (data !=null) {
        var display = '';

        var sorted_data = data.sort(function (a, b) {
            var x = a.id;
            var y = b.id;
            return x > y ? -1 : x < y ? 1 : 0;
        });

        $.each(data, function (key, response_data) {
            var color = "";
            switch (response_data.status.status) {
                case "created_acc":
                    color = "danger";
                    break;
                case "in_production":
                    color = "info";
                    break;
                case "pending":
                    color = "warning";
                    break
            }

            display += '<div id="' + response_data.computer_name + '" style="display:block; margin-left: 16px; margin-right: 16px; min-width: 200px " class="col-lg-11 col-md-9 terminal" name="terminals" data-search-name="' + response_data.computer_name + '" data-search-partner="' + response_data.country + response_data.partner_name + '" data-search-state="'+ response_data.in_state+'" data-search-time="'+ response_data.serial_created_tz +'" data-search-serial="'+response_data.serial_number+'">';
            display += '    <div class="box box-' + color + '  collapsed-box">';
            display += '        <button type="button" class="btn btn-box-tool class=col-lg-12" data-widget="collapse" style="width: 100%;" id="button1'+response_data.computer_name+'">';
            display += '        <div class="box-header with-border" style="float:left">';
            display += '            <h3 class="box-title"><i class="fa fa-tv" data-toggle="tooltip" data-html="true"></i><font size="2">  Kiosk: '+response_data.faze.Kiosk +'</font> | ' + response_data.computer_name + '</h3>';
            //display += '            <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>';
            //display += '            </div>';
            display += '        </div>';
            display += '        </button>';

            display += '        <div class="box-body">';
            // kar se bo pokazalo za pluskom
            display += '            <div class="box box-info box-solid">';
            display += '                <button type="button" class="btn btn-box-tool class=col-lg-12" data-widget="collapse" style="width: 100%;" id="button2'+response_data.computer_name+'">';
            display += '                <div class="box-header with-border" style="float: left">';
            display += '                    <h3 class="box-title">Show</h3>';
            //display += '            <!-- /.box-tools -->
            display += '                </div>';
            display += '        </button>';
            //display += '            <!-- /.box-header -->
            display += '                <div class="box-body">';
            display += '                    <form class="form-horizontal">';
            display += '                        <div class="form-group">';

            display += '                         <div class="form-group">';
            display += '                              <label for="input1" class="col-sm-3 control-label">Partner Name:</label>';
            display += '                              <div class="col-sm-4">';
            display += '                                  <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.partner_name" placeholder="Enter Partner Name" type="text" value="'+response_data.partner_name+'">';
            display += '                               </div>';
            display += '                          </div>';
            display += '                        <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Sub Partner Name:</label>';
            display += '                            <div class="col-sm-4">';
            display += '                                <input name="'+response_data.computer_name+'.faze" class="form-control" id="sub_partner" placeholder="Enter Sub Partner Name" type="text" value="'+response_data.faze.sub_partner+'">';
            display += '                            </div>';
            display += '                        </div>';
            display += '                        <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Shop Name:</label>';
            display += '                            <div class="col-sm-4">';
            display += '                                <input name="'+response_data.computer_name+'.faze" class="form-control focusout-suggest focusout-suggest2" id="shop_name" placeholder="Enter shop_name" type="text" value="'+response_data.faze.shop_name+'" data-toggle="tooltip" data-original-title="You can enter company/shop name.">';
            display += '                            </div>';
            display += '                        </div>';
            display += '                         <div class="form-group">';
            display += '                              <label for="input1" class="col-sm-3 control-label">Country:</label>';
            display += '                              <div class="col-sm-4">';
            display += '                                  <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.country" placeholder="Enter Country" type="text" value="'+response_data.country+'">';
            display += '                               </div>';
            display += '                            </div>';
            display += '                         <div class="form-group">';
            display += '                              <label for="input1" class="col-sm-3 control-label">City:</label>';
            display += '                              <div class="col-sm-4">';
            display += '                                  <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.city" placeholder="Enter City" type="text" value="'+response_data.city+'">';
            display += '                               </div>';
            display += '                            </div>';
            display += '                         <div class="form-group">';
            display += '                               <label for="input1" class="col-sm-3 control-label">Location Address:</label>';
            display += '                              <div class="col-sm-4">';
            display += '                                   <input name="'+ response_data.computer_name +'" class="form-control focusout-suggest focusout-suggest2 focusout-checker" id="'+response_data.computer_name+'.location_address" placeholder="Enter Location Address" type="text" value="'+response_data.location_address+'">';
            display += '                                <span id="'+response_data.computer_name+'.duplicate_label.location_address" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                             </div>';
            display += '                          </div>';

            display += '                         <div class="form-group">';
            display += '                                <label for="input1" class="col-sm-3 control-label">Computer Name:</label>';
            display += '                             <div class="col-sm-4">';
            display += '                                 <input name="'+ response_data.computer_name +'" class="form-control focusout-checker focusout-suggest1" id="'+response_data.computer_name+'.computer_name" placeholder="Enter Computer Name" type="text" value="'+response_data.computer_name+'">';
            display += '                                <span id="'+response_data.computer_name+'.duplicate_label.computer_name" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                             </div>';
            display += '                          </div>';
            display += '                         <div class="form-group">';
            display += '                             <label for="input1" class="col-sm-3 control-label">Serial Number:</label>';
            display += '                              <div class="col-sm-4">';
            display += '                                   <input name="'+ response_data.computer_name +'" class="form-control focusout-checker" id="'+response_data.computer_name+'.serial_number" placeholder="Enter Serial Number" type="text" value="'+response_data.serial_number+'">';
            display += '                                <span id="'+response_data.computer_name+'.duplicate_label.serial_number" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                             </div>';
            display += '                           </div>';

            var terminal_ty='';
            if(partners_specific[response_data.country+response_data.partner_name] != null) {
                $.each(partners_specific[response_data.country+response_data.partner_name].json_terminal_type, function (key, terminal_type) {
                    //console.log(terminal_type.terminal_type, response_data.terminal_type);
                    if (terminal_type.terminal_type === response_data.terminal_type){
                        terminal_ty +='<option selected value="'+terminal_type.terminal_type+'">'+terminal_type.terminal_type+'</option>';
                    }else{
                        terminal_ty +='<option value="'+terminal_type.terminal_type+'">'+terminal_type.terminal_type+'</option>';
                    }

                });
            }


            display += '                          <div class="form-group">';
            display += '                              <label for="input1" class="col-sm-3 control-label"> Terminal Type:</label>';
            display += '                                <div class="col-sm-4">';
            //display += '                                    <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.terminal_type"  type="text" value="'+response_data.terminal_type+'">';
            display += '                                    <select name="'+ response_data.computer_name +'"  class="form-control" id="'+response_data.computer_name+'.terminal_type"  type="text">'+terminal_ty+'</select>';
            display += '                              </div>';
            display += '                           </div>';

            display += '                           <div class="form-group">';
            display += '                             <label for="input1" class="col-sm-3 control-label">Cashdesk Username:</label>';
            display += '                              <div class="col-sm-9" style="padding-left: 0px;">';
            display += '                                <div class="col-sm-5">';
            display += '                                  <input name="'+ response_data.computer_name +'" class="form-control focusout-checker" id="'+response_data.computer_name+'.cd_un" placeholder="Enter Cashdesk Username" type="text" value="'+response_data.cd_un+'"  minlength="12" maxlength="12" data-toggle="tooltip" data-original-title="One cashdesk username per location.">';
            display += '                                </div>';
            display += '                              <div  class="col-sm-2">';
            display += '                                <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_cashuser" onclick="push_leftcash('+response_data.computer_name+')"><i class="fa fa-angle-left" data-action="Search icon"></i> Suggested</button>';
            display += '                              </div>';
            display += '                              <div class="col-sm-4">';
            display += '                                <input name="'+ response_data.computer_name +'" disabled class="form-control" id="'+response_data.computer_name+'.cd_un_suggest" placeholder="Enter Cashdesk Username" type="text" value=""  minlength="12" maxlength="12">';
            display += '                                <span id="'+response_data.computer_name+'.duplicate_label.cd_un" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                              </div>';
            display += '                             </div>';
            display += '                            </div>';

            display += '                           <div class="form-group">';
            display += '                                <label for="input1" class="col-sm-3 control-label">Cashdesk Password:</label>';
            display += '                                <div class="col-sm-4">';
            display += '                                    <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.cd_pw" placeholder="Enter Cashdesk Password" type="text" value="'+response_data.cd_pw+'">';
            display += '                             </div>';
            display += '                          </div>';
            display += '            <div class="form-group">';
            display += '                <label for="input1" class="col-sm-3 control-label">Oryx ID:</label>';
            display += '                <div class="col-sm-4">';
            display += '                    <input name="'+response_data.computer_name+'.faze" class="form-control" id="OryxID" placeholder="Enter Oryx ID" type="text" value="'+response_data.faze.OryxID+'">';
            display += '                </div>';
            display += '            </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Terminal Admin Username:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control focusout-checker" id="'+response_data.computer_name+'.ta_un" placeholder="Enter Terminal Admin Username" type="text" value="'+(response_data.ta_un)+'">';
            display += '                                <span id="'+response_data.computer_name+'.duplicate_label.ta_un" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                        </div>';
            display += '                    </div>';
            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Terminal Admin Password:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.ta_pw" placeholder="Enter Terminal Admin Password" type="text" value="'+(response_data.ta_pw)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Terminal Username:</label>';
            display += '                         <div class="col-sm-9" style="padding-left: 0px;">';
            display += '                             <div class="col-sm-5">';
            display += '                                <input name="'+ response_data.computer_name +'" class="form-control focusout-checker " id="'+response_data.computer_name+'.t_un" placeholder="Enter Terminal Username" type="text" value="'+(response_data.t_un)+'" data-toggle="tooltip" data-original-title="Same as computer name.">';
            display += '                             </div>';
            display += '                              <div  class="col-sm-2">';
            display += '                                <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.t_un" onclick="push_left_termin('+response_data.computer_name+')"><i class="fa fa-angle-left" data-action="Search icon"></i> Suggested</button>';
            display += '                              </div>';
            display += '                              <div class="col-sm-4">';
            display += '                                <input name="'+ response_data.computer_name +'" disabled class="form-control" id="'+response_data.computer_name+'.t_un_suggest" placeholder="Enter Cashdesk Username" type="text" value="'+response_data.t_un+'"  minlength="12" maxlength="12">';
            display += '                             <span id="'+response_data.computer_name+'.duplicate_label.t_un" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                            </div>';
            display += '                          </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Terminal Password:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.t_pw" placeholder="Enter Terminal Password" type="text" value="'+(response_data.t_pw)+'">';
            display += '                        </div>';
            display += '                    </div>';
            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Middleware Username:</label>';
            display += '                             <div class="col-sm-9" style="padding-left: 0px;">';
            display += '                                 <div class="col-sm-5">';
            display += '                                   <input name="'+ response_data.computer_name +'" class="form-control focusout-checker" id="'+response_data.computer_name+'.mw_un" placeholder="Enter Middleware Username" type="text" value="'+(response_data.mw_un)+'" data-toggle="tooltip" data-original-title="One middleware username per location.">';
            display += '                                 </div>';
            display += '                              <div  class="col-sm-2">';
            display += '                                <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_midwere" onclick="suggest_midwere('+response_data.computer_name+')"><i class="fa fa-angle-left" data-action="Search icon"></i> Suggested</button>';
            display += '                              </div>';
            display += '                              <div class="col-sm-4">';
            display += '                                <input name="'+ response_data.computer_name +'" disabled class="form-control" id="'+response_data.computer_name+'.mw_un_suggest" placeholder="Enter Cashdesk Username" type="text" value="'+response_data.mw_un+'"  minlength="12" maxlength="12">';
            display += '                                <span id="'+response_data.computer_name+'.duplicate_label.mw_un" style="color: #9f1730; padding-left: 10px; display: none;" >Duplicate!</span>';
            display += '                          </div>';
            display += '                             </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Middleware Password:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.mw_pw" placeholder="Enter Middleware Password" type="text" value="'+(response_data.mw_pw)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '            <div class="form-group">';
            display += '                <label for="input1" class="col-sm-3 control-label">Dealer:</label>';
            display += '                <div class="col-sm-9" style="padding-left: 0px;">';
            display += '                    <div class="col-sm-5">';
            display += '                        <input name="'+response_data.computer_name+'.faze" class="form-control" id="Dealer" placeholder="Enter Dealer" type="text" value="'+response_data.faze.Dealer+'" data-toggle="tooltip" data-original-title="Dealer.">';
            display += '                    </div>';
            display += '                    <div  class="col-sm-2">';
            display += '                        <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_dealer" onclick="suggest_dealer('+response_data.computer_name+')"><i class="fa fa-angle-left" data-action="Search icon"></i> Suggested</button>';
            display += '                    </div>';
            display += '                    <div class="col-sm-4">';
            display += '                        <input name="'+ response_data.computer_name +'" disabled class="form-control" id="'+response_data.computer_name+'.dealer_suggest" type="text" value="'+response_data.faze.Dealer+'" >';
            display += '                    </div>';
            display += '                </div>';
            display += '            </div>';

            display += '            <div class="form-group">';
            display += '                <label for="input1" class="col-sm-3 control-label">Kiosk:</label>';
            display += '                <div class="col-sm-9" style="padding-left: 0px;">';
            display += '                    <div class="col-sm-5">';
            display += '                        <input name="'+response_data.computer_name+'.faze" class="form-control" id="Kiosk" placeholder="Enter Kiosk" type="text" value="'+response_data.faze.Kiosk+'" data-toggle="tooltip" data-original-title="Name based on (Partner-SubPartner-Location). This field is printed on bet slip.">';
            display += '                    </div>';
            display += '                    <div  class="col-sm-2">';
            display += '                        <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_kiosk" onclick="suggest_kiosk('+response_data.computer_name+')"><i class="fa fa-angle-left" data-action="Search icon"></i> Suggested</button>';
            display += '                    </div>';
            display += '                    <div class="col-sm-4">';
            display += '                        <input name="'+ response_data.computer_name +'" disabled class="form-control" id="'+response_data.computer_name+'.kiosk_suggest" type="text" value="'+response_data.faze.Kiosk+'">';
            display += '                    </div>';
            display += '               </div>';
            display += '            </div>';

            display += '            <div class="form-group">';
            display += '                <label for="input1" class="col-sm-3 control-label">License:</label>';
            display += '                <div class="col-sm-4">';
            display += '                    <input name="'+response_data.computer_name+'.faze" class="form-control" id="License" placeholder="Enter F2 License" type="text" value="'+response_data.faze.License+'">';
            display += '                </div>';
            display += '            </div>';
            display += '            <div class="form-group">';
            display += '                <label for="input1" class="col-sm-3 control-label">Setup Id:</label>';
            display += '                <div class="col-sm-4">';
            display += '                    <input name="'+response_data.computer_name+'" class="form-control" id="'+response_data.computer_name+'.setup_id" placeholder="Enter Setup ID" type="text" value="'+response_data.setup_id+'">';
            display += '                </div>';
            display += '            </div>';

            display += '            <div class="form-group">';
            display += '                <label for="input1" class="col-sm-3 control-label">Comment:</label>';
            display += '                <div class="col-sm-4">';
            display += '                    <textarea class="form-control" rows="3" placeholder="Enter comments" name="'+response_data.computer_name+'" id="'+response_data.computer_name+'.service_comment" type="text">'+response_data.service_comment +'</textarea>';
            //display += '                    <input name="'+response_data.computer_name+'" class="form-control" id="'+response_data.service_comment+'" placeholder="Enter Setup ID" type="text" value="'+response_data.service_comment+'">';
            display += '                </div>';
            display += '            </div>';


            //----------------------------------------------------------
            display += '                        </div>'; //konec shown
            display += '                  </form>';
            display += '             </div>';
            //display += '        <!-- /.box-body -->
            display += '        </div>';

            //hidden
            display += '        <div class="box box-info box-solid collapsed-box">';
            display += '                <button type="button" class="btn btn-box-tool class=col-lg-12" data-widget="collapse" style="width: 100%;" id="button3'+response_data.computer_name+'">';
            display += '            <div class="box-header with-border " style="float: left">';
            display += '                 <h3 class="box-title">Details</h3>';
            //display += '            <!-- /.box-tools -->
            display += '            </div>';
            display += '            </button>';
            //display += '            <!-- /.box-header -->
            display += '        <div class="box-body">';
            display += '            <form class="form-horizontal">';
            display += '                <div class="form-group">';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Cage:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.cage" placeholder="Enter Cage" type="text" value="'+(response_data.cage)+'">';
            display += '                        </div>';
            display += '                    </div>';
            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Language Code:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.lang_code" placeholder="Enter Language Code" type="text" value="'+(response_data.lang_code)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">User interface language:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'.faze" class="form-control" id="'+response_data.computer_name+'.faze.ui_lang" placeholder="Enter user interface language" type="text" value="'+(response_data.faze.ui_lang)+'">';
            display += '                        </div>';
            display += '                    </div>';
            //console.log(response_data.faze, response_data.computer_name);
            display += '                          <div class="form-group">';
            display += '                               <label for="input1" class="col-sm-3 control-label">Game Code:</label>';
            display += '                               <div class="col-sm-4">';
            display += '                                   <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.game_code" placeholder="Enter Game Code" type="text" value="'+response_data.game_code+'">';
            display += '                                </div>';
            display += '                         </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Bet limit:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'.faze" class="form-control" id="'+response_data.computer_name+'.faze.Bet Limit" placeholder="Enter Setup ID" type="text" value="'+(response_data.faze["Bet Limit"])+'">';
            display += '                        </div>';
            display += '                    </div>';
            //------------------------orix-------------------------------
            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">ORYX Admin API:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.oryx_admin_api" placeholder="Enter ORYX Admin API" type="text" value="'+(response_data.oryx_admin_api)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">ORYX API Key:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.oryx_api_key" placeholder="Enter ORYX API Key" type="text" value="'+(response_data.oryx_api_key)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">ORYX Portal API:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.oryx_portal_api" placeholder="Enter ORYX Portal API" type="text" value="'+(response_data.oryx_portal_api)+'">';
            display += '                        </div>';
            display += '                    </div>';
            //-------------------------browsers-------------------------------
            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Primary Browser URL:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.primary_browser" placeholder="Enter Primary Browser URL" type="text" value="'+(response_data.primary_browser)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Secondary Browser:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.secondary_browser" placeholder="Enter Secondary Browser" type="text" value="'+(response_data.secondary_browser)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Printer Type:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.printer_type" placeholder="Enter Printer Type" type="text" value="'+(response_data.printer_type)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">prod_flag:</label>';
            display += '                        <div class="col-sm-4">';
            check = "";
            if(response_data.prod_flag) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.prod_flag" '+ check + '>';
            display += '                           </div>';
            display += '                          </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">sb_ws:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.sb_ws" placeholder="Enter sb_ws" type="text" value="'+(response_data.sb_ws)+'">';
            display += '                        </div>';
            display += '                    </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Betting API Handler:</label>';
            display += '                        <div class="col-sm-4">';
            display += '                            <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.sb_api_handler" placeholder="Enter Betting API Handler" type="text" value="'+(response_data.sb_api_handler)+'">';
            display += '                        </div>';
            display += '                    </div>';
            display += '                        <div class="form-group">';
            display += '                              <label for="input1" class="col-sm-3 control-label">IP Address:</label>';
            display += '                             <div class="col-sm-4">';
            display += '                                 <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.ip_address" placeholder="Enter IP Address" type="text" value="'+response_data.ip_address+'">';
            display += '                               </div>';
            display += '                       </div>';
            display += '                           <div class="form-group">';
            display += '                               <label for="input1" class="col-sm-3 control-label">State:</label>';
            display += '                                <div class="col-sm-4">';
            display += '                                    <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.in_state" placeholder="Enter State" type="text" value="'+response_data.in_state+'">';
            display += '                                </div>';
            display += '                         </div>';
            var date= response_data.serial_created_tz.split('T');
            display += '                          <div class="form-group">';
            display += '                               <label for="input1" class="col-sm-3 control-label">Date created:</label>';
            display += '                             <div class="col-sm-4">';
            display += '                                 <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.date[0]" type="text" value="'+date[0]+'">';
            display += '                              </div>';
            display += '                           </div>';

            display += '                           <div class="form-group">';
            display += '                               <label for="input1" class="col-sm-3 control-label">Production Confirmed:</label>';
            display += '                           <div class="col-sm-4">';
            check = "";
            if(response_data.prod_confirmed) {
                check = "checked";
            }
            //console.log("Production confirmed:", response_data.prod_confirmed);
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.prod_confirmed" '+ check + '>';
            display += '                             </div>';
            display += '                            </div>';

            display += '                          <div class="form-group">';
            display += '                             <label for="input1" class="col-sm-3 control-label">External IP:</label>';
            display += '                                 <div class="col-sm-4">';
            display += '                                    <input name="'+ response_data.computer_name +'" class="form-control" id="'+response_data.computer_name+'.ext_ip" placeholder="Enter External IP" type="text" value="'+response_data.ext_ip+'">';
            display += '                                 </div>';
            display += '                           </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Allow Rsync:</label>';
            display += '                        <div class="col-sm-4">';
            check = "";
            if(response_data.allow_rsync) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.allow_rsync" '+ check + '>';
            display += '                           </div>';
            display += '                          </div>';

            display += '                            <div class="form-group">';
            display += '                                <label for="input1" class="col-sm-3 control-label">Allow Vnc Monitor:</label>';
            display += '                                <div class="col-sm-4 ">';
            var check = "";
            if(response_data.allow_vnc_monitor) {
                check = "checked";
            }
            display += '                                    <input name="'+ response_data.computer_name +'" type="checkbox" id="'+response_data.computer_name+'.allow_vnc_monitor" '+ check + '>';
            display += '                                 </div>';
            display += '                            </div>';

            display += '                    <div class="form-group">';
            display += '                        <label for="input1" class="col-sm-3 control-label">Computer Vision Auto Initialize:</label>';
            display += '                        <div class="col-sm-4">';
            check = "";
            if(response_data.cv_ai) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.cv_ai" '+ check + '>';
            display += '                         </div>';
            display += '                      </div>';

            //skrit od faz
            $.each(response_data.faze, function (key, faza) {
                if(key != "Dealer" && key !="Kiosk" && key!="OryxID" && key!="shop_name" && key!="sub_partner" && key!= "License" && key!="setup_id"){
                    display += '               <input name="'+response_data.computer_name+'.faze" class="form-control" id="'+key+'" type="text" value="'+faza+'"style="display: none;">';
                }
            });

            display += '                </div>';
            display += '            </form>';
            display += '        </div>';
            display += '</div>';
            //display += '        <!-- /.box-body -->
            display += '        </div>';

            display += '            <div class="box-footer">';

            var terminal_s='';
            var allStatus = ["pending", "created_acc","in_production"];
            var prettyStatus = ["Pending","Created account","In production"];
            $.each(allStatus, function (key, status) {
                //console.log(terminal_type.terminal_type, response_data.terminal_type);
                if (response_data.status.status === status){
                    terminal_s+='<option selected value="'+status+'">'+prettyStatus[key]+'</option>';
                }else{
                    terminal_s +='<option value="'+status+'">'+prettyStatus[key]+'</option>';
                }
            });


            display += '            <div class="form-horizontal">';
            display += '                          <div class="form-group">';
            display += '                              <div class="col-sm-10" style="padding-left: 0px;">';
            display += '                                  <div  class="col-sm-2">';
            display += '                                    <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_barcode" onclick="get_barcode('+response_data.computer_name+')">Check barcode</button>';
            display += '                                  </div>';
            display += '                                  <div  class="col-sm-3">';
            display += '                                    <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_oryx_acc" onclick="get_oryx_acc('+response_data.computer_name+')">Check Oryx account</button>';
            display += '                                  </div>';
            display += '                                  <div  class="col-sm-3">';
            display += '                                    <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_bet_acc" onclick="get_bet_acc('+response_data.computer_name+')">Check betting account</button>';
            display += '                                  </div>';
            display += '                                  <div  class="col-sm-2">';
            display +='                                     <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_config" onclick="get_config('+response_data.computer_name+')">Generate config</button>';
            display += '                                 </div>';
            display += '                                 <div  class="col-sm-2">';
            display += '                                    <button type="button" class="btn btn-primary" id="'+response_data.computer_name+'.cheak_api_loc" onclick="get_api_loc('+response_data.computer_name+')">Get api location</button>';
            display += '                                </div>';
            display += '                             </div>';
            display += '                           </div>'
            display += '                           <hr>';

            display += '                          <div class="form-group" style="margin-top: 10px;">';

            display += '                        <label for="input1" class="col-sm-3 control-label">ORYX Account Created:</label>';
            display += '                        <div class="col-sm-1">';
            check = "";
            if(response_data.status.OAC) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.status.OAC" '+ check + ' style="margin-top: 10px">';
            display += '                           </div>';

            display += '                        <label for="input1" class="col-sm-3 control-label">Sports Betting Account Created:</label>';
            display += '                        <div class="col-sm-1">';
            check = "";
            if(response_data.status.SBAC) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.status.SBAC" '+ check + ' style="margin-top: 10px">';
            display += '                           </div>';

            display += '                              <label for="input1" class="col-sm-2 control-label" style="padding-top: 7px;";"> Terminal status:</label>';
            display += '                                <div class="col-sm-2">';
            display += '                                  <select name="'+ response_data.computer_name +'"  class="form-control" id="'+response_data.computer_name+'.status"  type="text">'+terminal_s+'</select>';
            display += '                              </div>';

            display += '                        <label for="input1" class="col-sm-3 control-label">Check oryx account:</label>';
            display += '                        <div class="col-sm-1">';
            check = "";
            if(response_data.status.COryx) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.status.COryx" '+ check + ' style="margin-top: 10px" checked>';
            display += '                           </div>';

            display += '                        <label for="input1" class="col-sm-3 control-label">Check betting account:</label>';
            display += '                        <div class="col-sm-1">';
            check = "";
            if(response_data.status.CBet) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.status.CBet" '+ check + ' style="margin-top: 10px" checked>';
            display += '                           </div>';

            display += '                        <label for="input1" class="col-sm-3 control-label">Generate config:</label>';
            display += '                        <div class="col-sm-1">';
            check = "";
            if(response_data.status.GeneConfig) {
                check = "checked";
            }
            display += '                             <input type="checkbox" id="'+response_data.computer_name+'.status.GeneConfig" '+ check + ' style="margin-top: 10px">';
            display += '                           </div>';
            display += '                              <hr>';
            display += '            </div>';
            display += '                              <hr>';
            var znacka="";
            if (response_data.prod_flag === true){
                znacka="disabled";
            }
            display += '                <button type="submit" style="float:left; margin-right: 10px " onclick="delete_acc('+response_data.computer_name+')" class="btn btn-danger '+znacka+'"  >Delete</button>';

            display += '                <button type="submit" class="btn btn-primary" onclick="edit_acc('+response_data.computer_name+');" style="float:right; margin-right: 10px ">Save changes</button>';
            display += '                           </div>'; //- od terminal form group
            //display += '            </div>';
            display += '                   </div>';
            display += '            </div>';
            display += '        </div>';

            //konec bodya
            display += '    </div>';
            display += '</div>';
            display += '</div>';

        });



        //console.log("Konec");
        document.getElementById("terminal_acc_status").innerHTML = display;

        $.each(data, function (key, response_data) {
            generateCashdeskUsername(response_data.computer_name);
            document.getElementById(response_data.computer_name + ".t_un_suggest").value = response_data.computer_name;
            generateMiddlewareUsername(response_data.computer_name);
        });

        // get last dealer and kiosk
        $.each(data, function (key, response_data) {
            localSession.call('si.nazvezi.telescope.get_last_account' , [response_data.country + response_data.partner_name]).then(
                function (response) {
                    //console.log(response);
                   document.getElementById(response_data.computer_name+".kiosk_suggest").value =response.faze.Kiosk;
                   document.getElementById(response_data.computer_name+".dealer_suggest").value =response.faze.Dealer;
                }
            );
        });


        //onfokus glej ce ime ze obstaja v bazi
        $(".focusout-checker").each(function(i, element) {
            $(element).focusout(function() {
                var identity = $(element).attr("id").split(".")[1];
                var c_name = $(element).attr("id").split(".")[0];
                //console.log(c_name);
                 cheakterminal_name(localSession, $(element).val(), identity,0, function(response){
                     var label = c_name+".duplicate_label."+identity;
                     document.getElementById(label).style.display = "";
                     document.getElementById(label).innerHTML = "";
                     document.getElementById(label).innerHTML = "This value is not in database.";
                 }, function (response) {
                     var found =0;
                     var label = c_name+".duplicate_label."+identity;
                     console.log(response);
                     $.each(response, function (key, response_data) {
                         console.log(response_data.computer_name);
                         if(response_data.computer_name == c_name) {
                             document.getElementById(label).style.display = "";
                             document.getElementById(label).innerHTML = "Found " + response.length + " match of current account. There is " + (response.length - 1 ) + " duplicates.";
                             found = 1;
                         }
                     });
                     if(found == 0 && response.length >-1 ){
                         document.getElementById(label).style.display = "";
                         document.getElementById(label).innerHTML = "Found "+response.length+ " match of DIFFERENT account!";
                     }
                 });
             });
        });

        var urlParams =url_param("tid");
        if(urlParams != "tid has no data." ){
            $("#prod_status").val('all').trigger("change");
           //console.log($("#prod_status").html());
            $("#time_added").val('all').trigger("change");
           // console.log($("#time_added").html());
            document.getElementById("button1"+urlParams).click();
            document.getElementById("button2"+urlParams).click();
            document.getElementById("button3"+urlParams).click();
            //document.getElementById(urlParams+".ip_address").scrollIntoView();
            $("html, body").animate({ scrollTop: $("#button1"+urlParams).offset().top }, 500);
        }

    }else {
        alertNotification('No data is recived from function "si.nazvezi.telescope.get_specific_terminals".');
    }
    filterTerminals();
}

//delete on click
function delete_acc(data) {

    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Delete terminal": function() {
                $( this ).dialog( "close" );
                console.log("DELETE");
                localSession.call('si.nazvezi.telescope.delete_terminal', [data.id]).then(
                    function (response) {
                        console.log("terminal deleted");
                        responseNotificationOK('Terminal was successfuly deleted!');
                        localSession.call('si.nazvezi.telescope.get_specific_terminals', [url_param("status")]).then(
                            function (data) {
                                console.log(data);
                                get_acc_data_by_status(localSession, data);
                            },
                            function (error) {
                                alertNotification('RPC function "si.nazvezi.telescope.get_specific_terminals" returned error: <b>'+error.error+'</b>');
                                console.log(error.error);
                            }
                        );
                    },
                    function (error) {
                        alertNotification('RPC function "si.nazvezi.telescope.delete_terminal" returned error: <b>'+error.error+'</b>');
                        console.log(error.error);
                    });
            },
            Cancel: function() {
                $( this ).dialog( "close" );
                console.log("CANCEL");
            }
        }
    });

}

//edit on click
function edit_acc(data) {
   // console.log(data.id);
    var computer_name = data.id;

    var a = $("#"+computer_name+".computer_name").val();
    //cheakterminal_name(connection.session, a);

        var input = document.getElementsByName(computer_name);
        var tempInput = [];
        for (var i = input.length - 1; i >= 0; i--){
            tempInput.push(input[i]);
        }
        var x =document.getElementById(computer_name+".location_address").value;
        x=x.replace(/'/g, '');
        x=x.replace(/"/g, '');
        //console.log(x);

        var podatki = {};

        podatki.allow_vnc_monitor =document.getElementById(computer_name+'.allow_vnc_monitor').checked;
        podatki.cd_un =document.getElementById(computer_name+'.cd_un').value;
        podatki.cd_pw =document.getElementById(computer_name+'.cd_pw').value;
        podatki.city =document.getElementById(computer_name+'.city').value;
        podatki.computer_name =document.getElementById(computer_name+'.computer_name').value;
        //podatki.serial_created_tz =document.getElementById(computer_name+'.serial_created_tz').value;
        podatki.ext_ip =document.getElementById(computer_name+'.ext_ip').value;
        podatki.game_code =document.getElementById(computer_name+'.game_code').value;
        podatki.in_state =document.getElementById(computer_name+'.in_state').value;
        podatki.ip_address =document.getElementById(computer_name+'.ip_address').value;
        podatki.location_address =document.getElementById(computer_name+'.location_address').value;
        podatki.prod_confirmed =document.getElementById(computer_name+'.prod_confirmed').checked;
        podatki.serial_number=document.getElementById(computer_name+'.serial_number').value;
        podatki.terminal_type =document.getElementById(computer_name+'.terminal_type').value;
        podatki.allow_rsync =document.getElementById(computer_name+'.allow_rsync').checked;
        podatki.cage=document.getElementById(computer_name+'.cage').value;
        podatki.cv_ai  =document.getElementById(computer_name+'.cv_ai').checked;
        podatki.lang_code =document.getElementById(computer_name+'.lang_code').value;
        podatki.mw_pw =document.getElementById(computer_name+'.mw_pw').value;
        podatki.mw_un =document.getElementById(computer_name+'.mw_un').value;
        podatki.oryx_admin_api =document.getElementById(computer_name+'.oryx_admin_api').value;
        podatki.oryx_api_key =document.getElementById(computer_name+'.oryx_api_key').value;
        podatki.oryx_portal_api =document.getElementById(computer_name+'.oryx_portal_api').value;
        podatki.primary_browser =document.getElementById(computer_name+'.primary_browser').value;
        podatki.printer_type =document.getElementById(computer_name+'.printer_type').value;
        podatki.sb_api_handler =document.getElementById(computer_name+'.sb_api_handler').value;
        podatki.secondary_browser =document.getElementById(computer_name+'.secondary_browser').value;
        podatki.setup_id =document.getElementById(computer_name+'.setup_id').value;
        podatki.t_un =document.getElementById(computer_name+'.t_un').value;
        podatki.t_pw =document.getElementById(computer_name+'.t_pw').value;
        podatki.ta_pw =document.getElementById(computer_name+'.ta_pw').value;
        podatki.ta_un =document.getElementById(computer_name+'.ta_un').value;
        podatki.sb_ws =document.getElementById(computer_name+'.sb_ws').value;
        podatki.prod_flag =document.getElementById(computer_name+'.prod_flag').checked;
        podatki.country =document.getElementById(computer_name+'.country').value;
        podatki.partner_name =document.getElementById(computer_name+'.partner_name').value;
        podatki.setup_id =document.getElementById(computer_name+'.setup_id').value;
        podatki.service_comment =document.getElementById(computer_name+'.service_comment').value;

        podatki.status ={};
        podatki.status.status = document.getElementById(computer_name+'.status').value;

        podatki.status.OAC =document.getElementById(computer_name+'.status.OAC').checked;
        podatki.status.SBAC =document.getElementById(computer_name+'.status.SBAC').checked;

        var calls = {}

        calls.GeneConfig =document.getElementById(computer_name+'.status.GeneConfig').checked;
        calls.CBet =document.getElementById(computer_name+'.status.CBet').checked;
        calls.COryx =document.getElementById(computer_name+'.status.COryx').checked;

        var faze_inputs = document.getElementsByName(computer_name + ".faze");
        podatki.faze = {}
        $.each(faze_inputs, function (key, element) {
            podatki.faze[element.id] = element.value;
        });
        session_update=true;
        terminal_ime = podatki.computer_name;
        var text="Do you want to save changes?<br>";
        document.getElementById("reload_confirm_text").innerHTML = text;
        document.getElementById("reload_confirm").title = "Terminal " + podatki.computer_name + " will be updated";
        $( "#reload_confirm" ).dialog({
            //title:terminal_ime,
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Save changes only": function() {
                    $( this ).dialog( "close" );
                    console.log("saved only");
                    localSession.call('si.nazvezi.telescope.update_terminal_account', [podatki, calls]).then(
                        function (data) {
                            console.log("Terminal je poosodobljen");
                            console.log(calls);
                            call_for_publish(podatki, calls, data);
                        },
                        function (error) {
                            alertNotification('RPC function "si.nazvezi.telescope.update_terminal_account" returned error: <b>'+error.error+'</b>');
                            console.log(error.error);
                        }
                    );
                    responseNotificationOK('Terminal was successfuly edited!');
                },
                "Save and reload": function() {
                    $( this ).dialog( "close" );
                    console.log("save and reload");
                    //reload page
                    localSession.call('si.nazvezi.telescope.update_terminal_account', [podatki, calls]).then(
                        function (data) {
                            console.log("Terminal je poosodobljen");
                            call_for_publish(podatki, calls, data);
                            localSession.call('si.nazvezi.telescope.get_specific_terminals', [url_param("status")]).then(
                                function (data) {
                                    console.log(data);
                                    get_acc_data_by_status(localSession, data);
                                },
                                function (error) {
                                    alertNotification('RPC function "si.nazvezi.telescope.get_specific_terminals" returned error: <b>'+error.error+'</b>');
                                    console.log(error.error);
                                }
                            );
                        },
                        function (error) {
                            alertNotification('RPC function "si.nazvezi.telescope.update_terminal_account" returned error: <b>'+error.error+'</b>');
                            console.log(error.error);
                        }
                    );
                    responseNotificationOK('Terminal was successfuly edited!');
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    console.log("CANCEL");
                }
            }
        });
}

function generateCashdeskUsername(c_name) {
    //console.log(c_name);
    var s_name="";
    var x = document.getElementsByName(c_name + ".faze");
    var l_name= document.getElementById(c_name + ".location_address").value;
    $.each(x, function(key, element){
        if(element.id == "shop_name" ){
            s_name=element.value;
        }
    });
    var shopName ="gp";
    var array= 'abcdefghijklmnopqrstuvwxyz0123456789'.split('');
    s_name=s_name.toLowerCase();
    var s_name1=s_name.split('');
    l_name=l_name.toLowerCase();
    var l_name1=l_name.split('');
    var found = false;
    var k = 0;
    if(s_name1.length >=2 && l_name1.length >=2){
        for(var i=0; i<3; i++){
            found = false;
            for(var j=0; j<array.length; j++){
                if(s_name1[i+k] == array[j]){
                    shopName +=s_name1[i];
                    found=true;
                }
            }if(found ==false){
                i--;
                k++;
            }
        }
        k = 0;
        for(var i=0; i<3; i++){
            found = false;
            for(var j=array.length; j>0; j--){
                if(l_name1[i+k] == array[j]){
                    shopName +=l_name1[i];
                    found=true;
                }
            }if(found ==false){
                i--;
                k++;
            }
        }
        shopName+="cash";
        //alert(shopName);
        cheakterminal_name(localSession, shopName, "cd_un",0, function (response) {
            //console.log("name is ok");
            document.getElementById(c_name + ".cd_un_suggest").value=shopName;
            var label = c_name+".duplicate_label.cd_un";
            document.getElementById(label).style.display = "none";
            document.getElementById(label).innerHTML = "";
        }, function (response) {
            var label = c_name+".duplicate_label.cd_un";
            document.getElementById(label).style.display = "";
            document.getElementById(label).innerHTML = "Found " + (response.length -1 )+ " duplicates.";
            document.getElementById(c_name + ".cd_un_suggest").value=shopName;
        });
    }
}

function generateMiddlewareUsername(c_name) {
    var s_name="";
    var x = document.getElementsByName(c_name + ".faze");
    var l_name= document.getElementById(c_name + ".location_address").value;
    $.each(x, function(key, element){
        if(element.id == "shop_name" ){
            s_name=element.value;
        }
    });

    var sshopName ="ta";
    var array= 'abcdefghijklmnopqrstuvwxyz0123456789'.split('');
    s_name=s_name.toLowerCase();
    var s_name1=s_name.split('');
    l_name=l_name.toLowerCase();
    var l_name1=l_name.split('');
    var found = false;
    var k = 0;
    if(s_name1.length >3 && l_name1.length >3) {
        for (var i = 0; i < 4; i++) {
            found = false;
            for (var j = 0; j < array.length; j++) {
                if (s_name1[i + k] == array[j]) {
                    sshopName += s_name1[i];
                    found = true;
                }
            }
            if (found == false) {
                i--;
                k++;
            }
        }
        k = 0;
        for (var i = 0; i < 4; i++) {
            found = false;
            for (var j = array.length; j > 0; j--) {
                if (l_name1[i + k] == array[j]) {
                    sshopName += l_name1[i];
                    found = true;
                }
            }
            if (found == false) {
                i--;
                k++;
            }
        }
        cheakterminal_name(localSession, sshopName, "mw_un", 0, function (response) {
            //console.log("name is ok");
            document.getElementById(c_name + ".mw_un_suggest").value = sshopName;
            var label = c_name + ".duplicate_label.mw_un";
            document.getElementById(label).style.display = "none";
            document.getElementById(label).innerHTML = "";
        }, function (response) {
            var label = c_name + ".duplicate_label.mw_un";
            document.getElementById(label).style.display = "";
            document.getElementById(label).innerHTML = "Found " + (response.length -1 ) + " duplicates.";
            document.getElementById(c_name + ".mw_un_suggest").value = sshopName;
        });
    }
}

//cheak_cashUser suggest name for cashdesk username + cheak if name is ok
function push_leftcash(data){
    document.getElementById(data[0].name + ".cd_un").value=document.getElementById(data[0].name+".cd_un_suggest").value;
}
function push_left_termin(data){
    document.getElementById(data[0].name + ".t_un").value=document.getElementById(data[0].name+".t_un_suggest").value;
}
function suggest_midwere(data){
    document.getElementById(data[0].name + ".mw_un").value=document.getElementById(data[0].name+".mw_un_suggest").value;
}
function suggest_kiosk(data){
    var faze = document.getElementsByName(data[0].name+".faze");
    $.each(faze, function(key, element){
        if(element.id == "Kiosk"){
            element.value=document.getElementById(data[0].name+".kiosk_suggest").value;
        }
    });
}
function suggest_dealer(data){
    var faze = document.getElementsByName(data[0].name+".faze");
    $.each(faze, function(key, element){
        if(element.id == "Dealer"){
            element.value=document.getElementById(data[0].name+".dealer_suggest").value;
        }
    });
}
function get_barcode(data){
    localSession.call('si.nazvezi.telescope.create_barcode', [data.id]).then(
        function (response) {
            window.open("data:application/pdf;base64," + escape(response));
            set_useraction_publish(localSession, localStorage.login_name,"get barcode",data.id+" ",'terminal' );
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.telescope.create_barcode" returned error: <b>'+error.error+'</b>');
            console.log(error.error);
        }
        );
}
function get_oryx_acc(data){
    responseNotification("Button Check Oryx account was clicked");
    var time = new Date();
    time = time.toString();
    var time1 = time.split(":");
    console.log(time1);
    var time = time1[0]+":"+time1[1];
    localSession.call('si.nazvezi.telescope.check_oryx_api', [data.id]).then(
        function (response) {
            if(response.success == true){
                console.log("alert");

                var text ="Balance: "+ response.balance+", API response time: "+response.ts +"s";
                responseNotificationOK('Terminal: '+ data.id+', Time : '+time +',</br> Balance: '+ response.balance+', API response time: '+response.ts +'s');
                set_useraction_publish(localSession, localStorage.login_name,"check Oryx account",data.id+" " +text,'terminal' );
            }
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.telescope.check_oryx_api" returned error: <b>'+error.error+'</b>');
            console.log(error.error);
        }
    );
}
function get_bet_acc(data){

    responseNotification("Button Check betting account was clicked");
    var time = new Date();
    time = time.toString();
    var time1 = time.split(":");
    console.log(time1);
    var time = time1[0]+":"+time1[1];
    localSession.call('si.nazvezi.telescope.check_sb_api', [data.id]).then(
        function (response) {
            if(response.success == true){
                console.log(response);

                var text =" Automat user: "+response.automat_user +", Login time: "+ response.login_time+"s, Tax: "+response.tax+", Tax time: "+response.tax_time ;
                responseNotificationOK('Terminal: '+ data.id+', Time : '+time +',</br><p> Automat user: '+response.automat_user +', Login time: '+ response.login_time+'s, </br>Tax: '+response.tax+', Tax time: '+response.tax_time +' </p>');
                set_useraction_publish(localSession, localStorage.login_name,"check betting account",data.id+" " +text, 'terminal');
            }
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.telescope.check_sb_api" returned error: <b>'+error.error+'</b>');
            console.log(error.error);
        }
    );
}
function get_config(data){
    responseNotification("Button Generate config was clicked");
    var time = new Date();
    time = time.toString();
    var time1 = time.split(":");
    var time = time1[0]+":"+time1[1];
    localSession.call('si.nazvezi.telescope.generate_config', [data.id]).then(
        function (response) {
            //console.log(response);
            responseNotificationOK('Terminal: '+ data.id+', Time : '+time +',</br>'+response);
            var text =response;
            set_useraction_publish(localSession, localStorage.login_name,"generate config",data.id+" " +text, 'terminal');
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.telescope.generate_config" returned error: <b>'+JSON.stringify(error)+'</b>');
            console.log(error.error);
        }
    );

}
function get_api_loc(data){
    responseNotification("Button Get api location was clicked");
    console.log(data);
    var time = new Date();
    time = time.toString();
    var time1 = time.split(":");
    console.log(time1);
    var time = time1[0]+":"+time1[1];
    var loc = document.getElementById(data.id+".location_address").value;
    var country = document.getElementById(data.id+".country").value;
    //var city = document.getElementById(data.id+".city").value;
    localSession.call('si.nazvezi.telescope.get_geo_location', [data.id, loc, country]).then(
        function (response) {
            console.log(response);
            var text = response.args[0];
            responseNotificationOK('Terminal: '+ data.id+', Time : '+time +',</br><p><a href="'+ response.args[0]+'">Coordinates link: </a> </br> X= '+response.args[1]+', Y= '+response.args[2] +'</p>');
            set_useraction_publish(localSession, localStorage.login_name,"get api location",data.id+" " +text, 'terminal');
        },
        function (error) {
            alertNotification('RPC function "si.nazvezi.telescope.get_geo_location" returned error: <b>'+error.error+'</b>');
            console.log(error.error);
        }
    );
}
function call_for_publish(podatki, calls, data){
    if(calls.CBet == true){
        if(typeof data.args[2].SB != 'string') {
            var response = data.args[2].SB[0];
            console.log(data.args);
            if (response.success == true) {
                console.log(response);
                var text = " Automat user: " + response.automat_user + ", Login time: " + response.login_time + "s, Tax: " + response.tax + ", Tax time: " + response.tax_time;
                set_useraction_publish(localSession, localStorage.login_name, "check betting account", podatki.computer_name + " " + text, 'terminal');
            } else  {
                console.log("Error za SB");
                var text= response.error;
                set_useraction_publish(localSession, localStorage.login_name,"check betting account",podatki.computer_name+" " +text,'terminal' );
            }
        }
        else{
            console.log("in error " );
            var response ="ERROR : "+ data.args[2].SB;
            var text= response;
            set_useraction_publish(localSession, localStorage.login_name,"check betting account",podatki.computer_name+" " +text,'terminal' );
        }
    }
    if(calls.COryx == true){
        if(typeof data.args[2].ORYX != 'string'){
            var response = data.args[2].ORYX[0];
            console.log(data.args);
            if(response.success == true){
                console.log(response);
                var text ="Balance: "+ response.balance+", API response time: "+response.ts +"s";
                set_useraction_publish(localSession, localStorage.login_name,"check Oryx account",podatki.computer_name+" " +text,'terminal' );
            }
        }else{
            var response = data.args[2].ORYX;
            console.log(data.args[2]);
            var text ="ERROR: "+ response;
            set_useraction_publish(localSession, localStorage.login_name,"check Oryx account",podatki.computer_name+" " +text,'terminal' );
        }
    }
    if(calls.GeneConfig == true){
        var response = data.args[2].CONFIG;
        var text =response ;
        set_useraction_publish(localSession, localStorage.login_name,"Generate config",podatki.computer_name+" " +text, 'terminal');
    }
}