/**
 * Created by marko on 13.11.2017.
 */
function display_sensors(data) {
    $.each(data, function (key, value) {
        console.log(key, value);
        var display = '';
        if(document.getElementById(value.mac) == null) {
            display += '<div id="' + value.client_id + '">';
            display += '    <div class="box">';
            display += '        <div class="box-header with-border">';
            display += '            <h3 class="box-title"><i class="fa fa-tv" data-toggle="tooltip" data-html="true" data-original-title="' + value.client_id + '"></i></h3>';
            display += '            <span class="btn label-box"> ' + value.mac + ' </span><br>';
            display += '            <div class="box-tools">';
            display += '                <span class="btn btn-default">' + value.device_name.toUpperCase() + '</span>';
            display += '                <span class="btn btn-default">' + value.sensor_id + '</span>';
            display += '            </div>';
            if (typeof value.details === 'object' && value.details !== null) {
                console.log(value.details);
                $.each(value.details, function (key, value2) {
                    if (value2 !== null) {
                        console.log(value2.GPIO, value2.name);
                        display += '<div class="checkbox" >';
                        display += '    <label>';
                        display += '    <input id="'+ key + '" class="switch" type="checkbox">';
                        display += '    channel: '+key+'';
                        display += '    <p class="location" onclick="listenForDoubleClick(this);" onblur="this.contentEditable=false;">' + value2.name+ '</p>';
                        //display += '    <input type="text" class="location" name="location" value="' + value2.name+ '">';
                        display += '</label>';
                        display += '</div>'}

                })
            }
            display += '<button name="button" class="btn bg-aqua" id="save_details">Save in Drave</button>';
            display += '        </div>';
            display += '        </div>';
            display += '    </div>';
            display += '</div>';
            document.getElementById("clients").innerHTML += display;

        } else {
            display += '    <div class="box">';
            display += '        <div class="box-header with-border">';
            display += '            <h3 class="box-title"><i class="fa fa-tv" data-toggle="tooltip" data-html="true" data-original-title="' + value.mac + '"></i></h3>';
            display += '            <span class="btn label-box"> ' + value.mac + ' </span><br>';
            display += '            <div class="box-tools">';
            display += '                <span class="btn btn-default">' + value.device_name.toUpperCase() + '</span>';
            display += '                <span class="btn btn-default">' + value.sensor_id + '</span>';
            display += '            </div>';
            if (typeof value.details === 'object' && value.details !== null) {
                console.log(value.details);
                $.each(value.details, function (key, value2) {
                    if (value2 !== null) {
                        console.log(value2.GPIO, value2.name);
                        display += '<div class="checkbox" >';
                        display += '    <label>';
                        display += '    <input id="'+ key + '" class="switch" type="checkbox">';
                        display += '    channel: '+key+'';
                        display += '    <p class="location" onclick="listenForDoubleClick(this);" onblur="this.contentEditable=false;">' + value2.name+ '</p>';
                        //display += '    <input type="text" class="location" name="location" value="' + value2.name+ '">';
                        display += '</label>';
                        display += '</div>'}

                })
            }
            display += '<button name="button" class="btn bg-aqua" id="save_details">Save in Drave</button>';
            display += '        </div>';

            display += '        </div>';
            display += '    </div>';

            display += '</div>';
            document.getElementById(value.mac).innerHTML += display;
        }

    });


}

function get_devices(session){
    session.call('sensors_on_devices').then(
        function (response) {
            if (response){
                var sensors_list = document.getElementById('clients');
                $.each(response, function (device_id, sensor) {
                    //create device box
                    var device_box_wrapper = document.createElement('div');
                    device_box_wrapper.classList.add('col-md-6');
                    device_box_wrapper.id = device_id;
                    var sensor_box_wrapper = document.createElement('div');
                    sensor_box_wrapper.classList.add('box', 'box-default');
                    device_box_wrapper.appendChild(sensor_box_wrapper);
                    var sensor_box_header = document.createElement('div');
                    sensor_box_header.classList.add('box-header', 'with-border');
                    sensor_box_wrapper.appendChild(sensor_box_header);
                    var sensor_box_title = document.createElement('h3');
                    sensor_box_title.classList.add('box-title');
                    sensor_box_title.textContent = "Device_id: "+device_id;
                    sensor_box_header.appendChild(sensor_box_title);
                    var sensor_box_tools = document.createElement('div');
                    sensor_box_tools.classList.add('box-tools', 'pull-right');
                    sensor_box_header.appendChild(sensor_box_tools);
                    var sensor_box_tool_button = document.createElement('button');
                    sensor_box_tool_button.classList.add('box-tools', 'pull-right');
                    sensor_box_tool_button.setAttribute('data-widget', 'collapse');
                    sensor_box_tools.appendChild(sensor_box_tool_button);
                    var sensor_box_button = document.createElement('i');
                    sensor_box_button.classList.add('fa', 'fa-minus');
                    sensor_box_tool_button.appendChild(sensor_box_button);
                    var sensor_box_body = document.createElement('div');
                    sensor_box_body.classList.add("box-body");
                    sensor_box_body.style.textAlign = "center";
                    sensor_box_wrapper.appendChild(sensor_box_body);
                    if (typeof sensor === 'object') {
                        $.each(sensor, function (sensor_id, sensor_details) {
                            console.log("id =>", sensor_id);
                            console.log("details =>", sensor_details);
                            // create sensor box in device box
                            var sensor_box = document.createElement('div');
                            sensor_box.classList.add('box', 'box-default');
                            sensor_box.id = sensor_id;
                            // header wrapper
                            var box_header = document.createElement('div');
                            box_header.classList.add('box-header', 'with-border');
                            var box_title = document.createElement('h3');
                            box_title.classList.add('box-title');
                            box_title.textContent = "Sensor_id: "+sensor_id;
                            var box_tools = document.createElement('div');
                            box_tools.classList.add('box-tools', 'pull-right');
                            var box_tool_button = document.createElement('button');
                            box_tool_button.classList.add('box-tools', 'pull-right');
                            box_tool_button.setAttribute('data-widget', 'collapse');
                            var box_button = document.createElement('i');
                            box_button.classList.add('fa', 'fa-minus');
                            var box_body = document.createElement('div');
                            box_body.classList.add("box-body");
                            box_body.style.textAlign = "center";
                            sensor_box_body.appendChild(sensor_box);
                            sensor_box.appendChild(box_header);
                            box_header.appendChild(box_title);
                            box_header.appendChild(box_tools);
                            box_tools.appendChild(box_tool_button);
                            box_tool_button.appendChild(box_button);
                            sensor_box.appendChild(box_body);
                            if (typeof sensor_details === 'object') {
                                $.each(sensor_details, function (key, value) {
                                    if (typeof value === 'object' && value !== null) {
                                        if (sensor_details.sensor_id === "RELAY") {
                                            var table = document.createElement('table');
                                            table.classList.add('table', 'table-bordered');
                                            table.id = sensor_details.sensor_id;
                                            var table_body = document.createElement('tbody');
                                            table.appendChild(table_body);
                                            var first_row = document.createElement('tr');
                                            table_body.appendChild(first_row);
                                            var channel = document.createElement('th');
                                            channel.textContent = 'Channel';
                                            channel.style.width = "15px";
                                            first_row.appendChild(channel);
                                            var state = document.createElement('th');
                                            state.textContent = 'State';
                                            state.style.width = "120px";
                                            first_row.appendChild(state);
                                            var location = document.createElement('th');
                                            location.textContent = 'Location';
                                            first_row.appendChild(location);
                                            var pin = document.createElement('th');
                                            pin.textContent = 'Pin';
                                            pin.style.width = "15px";
                                            first_row.appendChild(pin);

                                            $.each(value, function (details_key, details_value) {
                                                var row = document.createElement('tr');
                                                var channel = document.createElement('td');
                                                channel.textContent = details_key;
                                                row.appendChild(channel);
                                                var state = document.createElement('td');
                                                var checkbox_div = document.createElement('div');
                                                checkbox_div.classList.add("checkbox");
                                                if (details_value.state === true){ checkbox_div.classList.add("bootstrap-switch-handle-on"); console.log("ENABLED");}
                                                else { checkbox_div.classList.add("bootstrap-switch-handle-off"); console.log("Disabled");}
                                                var checkbox_label = document.createElement('label');
                                                var checkbox = document.createElement('input');
                                                checkbox.classList.add('switch');
                                                checkbox.type = 'checkbox';
                                                checkbox.id = details_key;
                                                row.appendChild(state);
                                                state.appendChild(checkbox_div);
                                                checkbox_div.appendChild(checkbox_label);
                                                checkbox_label.appendChild(checkbox);
                                                var location = document.createElement('td');
                                                var location_textinput = document.createElement('p');
                                                location_textinput.classList.add("location");
                                                location_textinput.onclick = function () { listenForDoubleClick(this); };
                                                location_textinput.onblur = function () { this.contentEditable=false; };
                                                location_textinput.textContent = details_value.name;
                                                location.appendChild(location_textinput);
                                                row.appendChild(location);

                                                var pin = document.createElement('td');
                                                pin.textContent = details_value.GPIO;
                                                row.appendChild(pin);
                                                table_body.appendChild(row);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });
                                            box_body.appendChild(table);
                                        } else if (sensor_details.sensor_id === "LED") {
                                            $.each(value, function (details_key, details_value) {
                                                console.log(device_id);
                                                var box_body_button_display = document.createElement('button');
                                                box_body_button_display.textContent = details_key;
                                                box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                                                box_body_button_display.style.margin = "5px";
                                                // box_body_button_display.id = "I2C_DISPLAY";
                                                box_body.appendChild(box_body_button_display);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });
                                        } else if (sensor_details.sensor_id === "TEMP_DALLAS") {
                                            $.each(value, function (details_key, details_value) {
                                                console.log(device_id);
                                                var box_body_button_display = document.createElement('button');
                                                box_body_button_display.textContent = details_key;
                                                box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                                                box_body_button_display.style.margin = "5px";
                                                // box_body_button_display.id = "I2C_DISPLAY";
                                                box_body.appendChild(box_body_button_display);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });

                                        }else if (sensor_details.sensor_id === "I2C_DISPLAY") {
                                            $.each(value, function (details_key, details_value) {
                                                console.log(device_id);
                                                var box_body_button_display = document.createElement('button');
                                                box_body_button_display.textContent = details_key;
                                                box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                                                box_body_button_display.style.margin = "5px";
                                                // box_body_button_display.id = "I2C_DISPLAY";
                                                box_body.appendChild(box_body_button_display);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });

                                        }
                                    }
                                });
                            }
                        });
                    }
                    sensors_list.appendChild(device_box_wrapper);
                });
                var buttons = document.querySelectorAll('#clients .initialize');
                Array.from(buttons).forEach(function (button) {
                    button.addEventListener('click', function (e) {
                        console.log(e.target.id);
                        //console.log(e.target.parentElement.parentElement.parentElement.id);
                        var topic = e.target.parentElement.parentElement.id;
                        // initialize_sensor(session, topic, JSON.stringify({"sensor": e.target.id, "new": true, "pins": {"GPIO": 34, "name": null, "threshold": 1200}}));
                        console.log(topic);
                        // console.log()
                    })

                });
            }
        }
    ).done(function () {
            $('.switch').bootstrapSwitch();
            $('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                var device_id = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id;
                var sensor_id = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id;
                sensor_switch(session, sensor_id, device_id, event.target.id)
            });
        }

    );
}

function add_sensor(data) {
    var sensors_list = document.getElementById('clients').getElementsByTagName('div');
    console.log(sensors_list)

}

function registered_sensors(session){

}

function unregistered_sensors(session) {
    session.call('return_uninitialized_devices').then(
        function (response) {
            if (response){
                $.each(response, function (key, response_data) {
                    var sensors_list = document.getElementById('clients');
                    var box_wrapper = document.createElement('div');
                    box_wrapper.classList.add('col-md-6');
                    box_wrapper.id = response_data;
                    var box = document.createElement('div');
                    box.classList.add('box', 'box-default');
                    var box_header = document.createElement('div');
                    box_header.classList.add('box-header', 'with-border');
                    var box_title = document.createElement('h3');
                    box_title.classList.add('box-title');
                    box_title.textContent = "Device_id: "+response_data;
                    var box_tools = document.createElement('div');
                    box_tools.classList.add('box-tools', 'pull-right');
                    var box_tool_button = document.createElement('button');
                    box_tool_button.classList.add('box-tools', 'pull-right');
                    box_tool_button.setAttribute('data-widget', 'collapse');
                    var box_button = document.createElement('i');
                    box_button.classList.add('fa', 'fa-minus');
                    var box_body = document.createElement('div');
                    box_body.classList.add("box-body");
                    box_body.style.textAlign = "center";
                    // enable display
                    var box_body_button_display = document.createElement('button');
                    box_body_button_display.textContent = "Display";
                    box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize','col-md-3');
                    box_body_button_display.style.margin= "5px";
                    box_body_button_display.id = "I2C_DISPLAY";
                    //enable Relay
                    var box_body_button_relay = document.createElement('button');
                    box_body_button_relay.textContent = "Relay";
                    box_body_button_relay.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_relay.style.margin= "5px";
                    box_body_button_relay.id = "RELAY";
                    //enable DHT11
                    var box_body_button_temp_dht11 = document.createElement('button');
                    box_body_button_temp_dht11.textContent = "Temp. sensor DHT11";
                    box_body_button_temp_dht11.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_temp_dht11.style.margin= "5px";
                    box_body_button_temp_dht11.id = "TEMP_DH11";
                    //enable Dallas
                    var box_body_button_temp_dallas = document.createElement('button');
                    box_body_button_temp_dallas.textContent = "Temp. sensor Dallas";
                    box_body_button_temp_dallas.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_temp_dallas.style.margin= "5px";
                    box_body_button_temp_dallas.id = "TEMP_DALLAS";
                    //enable IR sensor
                    var box_body_button_ir = document.createElement('button');
                    box_body_button_ir.textContent = "IR sensor";
                    box_body_button_ir.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_ir.style.margin= "5px";
                    box_body_button_ir.id = "IR_SENSOR";
                    //enable IR sensor
                    var box_body_button_co = document.createElement('button');
                    box_body_button_co.textContent = "CO sensor";
                    box_body_button_co.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_co.style.margin= "5px";
                    box_body_button_co.id = "CO_SENSOR";
                    //enable humidity sensor
                    var box_body_button_humidity = document.createElement('button');
                    box_body_button_humidity.textContent = "Humidity sensor";
                    box_body_button_humidity.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_humidity.style.margin= "5px";
                    box_body_button_humidity.id = "HUMIDITY_SENSOR";
                    //enable led
                    var box_body_button_led = document.createElement('button');
                    box_body_button_led.textContent = "LED";
                    box_body_button_led.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_led.style.margin= "5px";
                    box_body_button_led.id = "LED";
                    //append to DOM
                    box_wrapper.appendChild(box);
                    box.appendChild(box_header);
                    box_header.appendChild(box_title);
                    box_header.appendChild(box_tools);
                    box_tools.appendChild(box_tool_button);
                    box_tool_button.appendChild(box_button);
                    box.appendChild(box_body);
                    box_body.appendChild(box_body_button_display);
                    box_body.appendChild(box_body_button_relay);
                    box_body.appendChild(box_body_button_temp_dht11);
                    box_body.appendChild(box_body_button_temp_dallas);
                    box_body.appendChild(box_body_button_ir);
                    box_body.appendChild(box_body_button_co);
                    box_body.appendChild(box_body_button_humidity);
                    box_body.appendChild(box_body_button_led);
                    sensors_list.appendChild(box_wrapper);
                });
                var buttons = document.querySelectorAll('#clients .initialize');
                Array.from(buttons).forEach(function (button) {
                    button.addEventListener('click', function (e) {
                        console.log(e.target.id);
                        console.log(e.target.parentElement.parentElement.parentElement.id);
                        var topic = e.target.parentElement.parentElement.parentElement.id;

                        var modal_title = document.getElementById('modal-title');
                        modal_title.textContent = "Add device :" + e.target.id;
                        var modal_body = document.getElementById('modal-body');
                        while (modal_body.firstChild) {
                            modal_body.removeChild(modal_body.firstChild);
                        }
                        var modal_form = document.createElement('form');
                        var input = document.createElement('input');
                        input.setAttribute("type", "text");
                        input.setAttribute("value", "Hello World!");
                        modal_form.appendChild(input);
                        modal_body.appendChild(modal_form);
                        var modal_add = document.getElementById('add-device');
                        modal_add.onclick = function () { initialize_sensor(session, topic, JSON.stringify({"sensor": e.target.id, "new": true, "pins": {"GPIO": 26, "name": null, "threshold": 1200}})); };
                        console.log(modal_add);
                        $('#myModal').modal('show');
                    })
                });
                // var buttons2 = document.querySelectorAll('#clients .initialize');
                // Array.from(buttons2).forEach(function (button) {
                //     button.addEventListener('click', function (e) {
                //         console.log(e.target.id);
                //         //console.log(e.target.parentElement.parentElement.parentElement.id);
                //         var topic = e.target.parentElement.parentElement.parentElement.id;
                //         var name = "Not available";
                //         var pins = {
                //             '1': {'GPIO': 15, 'state': false, 'name': name},
                //             '2': {'GPIO': 13, 'state': false, 'name': name},
                //             '3': {'GPIO': 14, 'state': false, 'name': name},
                //             '4': {'GPIO': 2, 'state': false, 'name': name},
                //             '5': {'GPIO': 0, 'state': false, 'name': name},
                //             '6': {'GPIO': 16, 'state': false, 'name': name},
                //             '7': {'GPIO': 25, 'state': false, 'name': name},
                //             '8': {'GPIO': 26, 'state': false, 'name': name}
                //         };
                //         initialize_sensor(session, topic, JSON.stringify({"sensor": e.target.id, "new": true, "pins": pins}));
                //     })
                // });
            }
        })
}

function initialize_sensor(session, topic, sensor){
    //console.log(topic, sensor);
    session.call('mqtt_publish', ['initialize/' + topic, sensor]).then(
        function (response) {
            console.log(response)

        }
    )
}

function sensor_switch(session, device, device_id, channel){
    //console.log(topic, sensor);
    session.call('mqtt_publish', ['esp32/relay/' + device_id, JSON.stringify({"sensor": device, "channel": parseInt(channel), "value": "toggle"})]).then(
        function (response) {
            console.log(response)

        }
    )
}



/*
relay pins
 ch1 = 15
 ch2 = 13
 ch3 = 14
 ch4 = 2
 ch5 = 0
 ch6 = 16
 ch7 = 25
 ch8 = 26

 {
 '1': {'GPIO': 15, 'state': False, 'name': null},
 '2': {'GPIO': 13, 'state': False, 'name': null},
 '3': {'GPIO': 14, 'state': False, 'name': null},
 '4': {'GPIO': 2, 'state': False, 'name': null},
 '5': {'GPIO': 0, 'state': False, 'name': null},
 '6': {'GPIO': 16, 'state': False, 'name': null},
 '7': {'GPIO': 25, 'state': False, 'name': null},
 '8': {'GPIO': 26, 'state': False, 'name': null},
 }
 must send on relay initialization

 */