/**
 * Created by marko on 13.11.2017.
 */
function display_sensors(data) {
    $.each(data, function (key, value) {
        console.log(key, value);
        var display = '';
        if(document.getElementById(value.mac) == null) {
            display += '<div id="' + value.client_id + '">';
            display += '    <div class="box">';
            display += '        <div class="box-header with-border">';
            display += '            <h3 class="box-title"><i class="fa fa-tv" data-toggle="tooltip" data-html="true" data-original-title="' + value.client_id + '"></i></h3>';
            display += '            <span class="btn label-box"> ' + value.mac + ' </span><br>';
            display += '            <div class="box-tools">';
            display += '                <span class="btn btn-default">' + value.device_name.toUpperCase() + '</span>';
            display += '                <span class="btn btn-default">' + value.sensor_id + '</span>';
            display += '            </div>';
            if (typeof value.details === 'object' && value.details !== null) {
                console.log(value.details);
                $.each(value.details, function (key, value2) {
                    if (value2 !== null) {
                        console.log(value2.GPIO, value2.name);
                        display += '<div class="checkbox" >';
                        display += '    <label>';
                        display += '    <input id="'+ key + '" class="switch" type="checkbox">';
                        display += '    channel: '+key+'';
                        display += '    <p class="location" onclick="listenForDoubleClick(this);" onblur="this.contentEditable=false;">' + value2.name+ '</p>';
                        //display += '    <input type="text" class="location" name="location" value="' + value2.name+ '">';
                        display += '</label>';
                        display += '</div>'}

                })
            }
            display += '<button name="button" class="btn bg-aqua" id="save_details">Save in Drave</button>';
            display += '        </div>';
            display += '        </div>';
            display += '    </div>';
            display += '</div>';
            document.getElementById("clients").innerHTML += display;

        } else {
            display += '    <div class="box">';
            display += '        <div class="box-header with-border">';
            display += '            <h3 class="box-title"><i class="fa fa-tv" data-toggle="tooltip" data-html="true" data-original-title="' + value.mac + '"></i></h3>';
            display += '            <span class="btn label-box"> ' + value.mac + ' </span><br>';
            display += '            <div class="box-tools">';
            display += '                <span class="btn btn-default">' + value.device_name.toUpperCase() + '</span>';
            display += '                <span class="btn btn-default">' + value.sensor_id + '</span>';
            display += '            </div>';
            if (typeof value.details === 'object' && value.details !== null) {
                console.log(value.details);
                $.each(value.details, function (key, value2) {
                    if (value2 !== null) {
                        console.log(value2.GPIO, value2.name);
                        display += '<div class="checkbox" >';
                        display += '    <label>';
                        display += '    <input id="'+ key + '" class="switch" type="checkbox">';
                        display += '    channel: '+key+'';
                        display += '    <p class="location" onclick="listenForDoubleClick(this);" onblur="this.contentEditable=false;">' + value2.name+ '</p>';
                        //display += '    <input type="text" class="location" name="location" value="' + value2.name+ '">';
                        display += '</label>';
                        display += '</div>'}

                })
            }
            display += '<button name="button" class="btn bg-aqua" id="save_details">Save in Drave</button>';
            display += '        </div>';

            display += '        </div>';
            display += '    </div>';

            display += '</div>';
            document.getElementById(value.mac).innerHTML += display;
        }

    });


}

function get_devices(session){
    session.call('sensors_on_devices').then(
        function (response) {
            if (response){

                var sensors_list = document.getElementById('clients');
                $.each(response, function (device_id, sensor) {
                    //create device box
                    var device_box_wrapper = document.createElement('div');
                    device_box_wrapper.classList.add('col-md-6');
                    device_box_wrapper.id = device_id;
                    console.log(sensor);

                    if (typeof sensor === 'object') {
                        $.each(sensor, function (sensor_id, sensor_details) {
                            console.log("id =>", sensor_id);
                            console.log("details =>", sensor_details);
                            // create sensor box in device box
                            var sensor_box = document.createElement('div');
                            sensor_box.classList.add('box', 'box-default');
                            // header wrapper
                            var box_header = document.createElement('div');
                            box_header.classList.add('box-header', 'with-border');
                            var box_title = document.createElement('h3');
                            box_title.classList.add('box-title');
                            box_title.textContent = "Device_id: "+sensor_id+' '+device_id;

                            var box_tools = document.createElement('div');
                            box_tools.classList.add('box-tools', 'pull-right');


                            var box_tool_button = document.createElement('button');
                            box_tool_button.classList.add('box-tools', 'pull-right');
                            box_tool_button.setAttribute('data-widget', 'collapse');

                            var box_button = document.createElement('i');
                            box_button.classList.add('fa', 'fa-minus');

                            var box_body = document.createElement('div');
                            box_body.classList.add("box-body");
                            box_body.style.textAlign = "center";

                            device_box_wrapper.appendChild(sensor_box);
                            sensor_box.appendChild(box_header);
                            box_header.appendChild(box_title);
                            box_header.appendChild(box_tools);
                            box_tools.appendChild(box_tool_button);
                            box_tool_button.appendChild(box_button);
                            sensor_box.appendChild(box_body);

                            if (typeof sensor_details === 'object') {
                                $.each(sensor_details, function (key, value) {
                                    console.log(key);
                                    console.log(value);
                                    console.log("sensor =>" + sensor_id);
                                    console.log("device =>" + device_id);
                                    console.log("sensor type =>" + sensor_details.sensor_id);
                                    if (typeof value === 'object' && value !== null) {
                                        if (sensor_details.sensor_id === "RELAY") {
                                            $.each(value, function (details_key, details_value) {
                                                console.log(device_id);

                                                var box_body_button_display = document.createElement('button');
                                                box_body_button_display.textContent = details_key;
                                                box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                                                box_body_button_display.style.margin = "5px";
                                                box_body_button_display.id = "I2C_DISPLAY";

                                                box_body.appendChild(box_body_button_display);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });
                                        } else if (sensor_details.sensor_id === "LED") {
                                            $.each(value, function (details_key, details_value) {
                                                console.log(device_id);

                                                var box_body_button_display = document.createElement('button');
                                                box_body_button_display.textContent = details_key;
                                                box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                                                box_body_button_display.style.margin = "5px";
                                                box_body_button_display.id = "I2C_DISPLAY";

                                                box_body.appendChild(box_body_button_display);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });

                                        } else if (sensor_details.sensor_id === "TEMP_DALLAS") {
                                            $.each(value, function (details_key, details_value) {
                                                console.log(device_id);

                                                var box_body_button_display = document.createElement('button');
                                                box_body_button_display.textContent = details_key;
                                                box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                                                box_body_button_display.style.margin = "5px";
                                                box_body_button_display.id = "I2C_DISPLAY";

                                                box_body.appendChild(box_body_button_display);
                                                console.log(details_key);
                                                console.log(details_value);
                                            });

                                        }
                                    }
                                });
                            }
                        });
                    }
                    sensors_list.appendChild(device_box_wrapper);
                });

            }
        }
    )
}

function add_sensor(data) {
    var sensors_list = document.getElementById('clients').getElementsByTagName('div');
    console.log(sensors_list)

}

function registered_sensors(session){

}

function unregistered_sensors(session) {
    session.call('return_uninitialized_devices').then(
        function (response) {
            if (response){
                $.each(response, function (key, response_data) {
                    var sensors_list = document.getElementById('clients');

                    var box_wrapper = document.createElement('div');
                    box_wrapper.classList.add('col-md-6');
                    box_wrapper.id = response_data;

                    var box = document.createElement('div');
                    box.classList.add('box', 'box-default');

                    var box_header = document.createElement('div');
                    box_header.classList.add('box-header', 'with-border');

                    var box_title = document.createElement('h3');
                    box_title.classList.add('box-title');
                    box_title.textContent = "Device_id: "+response_data;


                    var box_tools = document.createElement('div');
                    box_tools.classList.add('box-tools', 'pull-right');


                    var box_tool_button = document.createElement('button');
                    box_tool_button.classList.add('box-tools', 'pull-right');
                    box_tool_button.setAttribute('data-widget', 'collapse');

                    var box_button = document.createElement('i');
                    box_button.classList.add('fa', 'fa-minus');

                    var box_body = document.createElement('div');
                    box_body.classList.add("box-body");
                    box_body.style.textAlign = "center";
                    // '<button name="button" class=" " id="save_details">Save in Drave</button>';

                    // enable display
                    var box_body_button_display = document.createElement('button');
                    box_body_button_display.textContent = "Display";
                    box_body_button_display.classList.add('btn', 'bg-aqua', 'initialize','col-md-3');
                    box_body_button_display.style.margin= "5px";;
                    box_body_button_display.id = "I2C_DISPLAY"

                    //enable Relay
                    var box_body_button_relay = document.createElement('button');
                    box_body_button_relay.textContent = "Relay";
                    box_body_button_relay.classList.add('btn', 'bg-aqua', 'initialize_relay', 'col-md-3');
                    box_body_button_relay.style.margin= "5px";
                    box_body_button_relay.id = "RELAY";

                    //enable DHT11
                    var box_body_button_temp_dht11 = document.createElement('button');
                    box_body_button_temp_dht11.textContent = "Temp. sensor DHT11";
                    box_body_button_temp_dht11.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_temp_dht11.style.margin= "5px";
                    box_body_button_temp_dht11.id = "TEMP_DH11";

                    //enable Dallas
                    var box_body_button_temp_dallas = document.createElement('button');
                    box_body_button_temp_dallas.textContent = "Temp. sensor Dallas";
                    box_body_button_temp_dallas.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_temp_dallas.style.margin= "5px";
                    box_body_button_temp_dallas.id = "TEMP_DALLAS";

                    //enable IR sensor
                    var box_body_button_ir = document.createElement('button');
                    box_body_button_ir.textContent = "IR sensor";
                    box_body_button_ir.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_ir.style.margin= "5px";
                    box_body_button_ir.id = "IR_SENSOR";

                    //enable IR sensor
                    var box_body_button_co = document.createElement('button');
                    box_body_button_co.textContent = "CO sensor";
                    box_body_button_co.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_co.style.margin= "5px";
                    box_body_button_co.id = "CO_SENSOR";

                    //enable humidity sensor
                    var box_body_button_humidity = document.createElement('button');
                    box_body_button_humidity.textContent = "Humidity sensor";
                    box_body_button_humidity.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_humidity.style.margin= "5px";
                    box_body_button_humidity.id = "HUMIDITY_SENSOR";

                    //enable led
                    var box_body_button_led = document.createElement('button');
                    box_body_button_led.textContent = "LED";
                    box_body_button_led.classList.add('btn', 'bg-aqua', 'initialize', 'col-md-3');
                    box_body_button_led.style.margin= "5px";
                    box_body_button_led.id = "LED";


                    //append to DOM
                    box_wrapper.appendChild(box);
                    box.appendChild(box_header);
                    box_header.appendChild(box_title);
                    box_header.appendChild(box_tools);
                    box_tools.appendChild(box_tool_button);
                    box_tool_button.appendChild(box_button);
                    box.appendChild(box_body);

                    box_body.appendChild(box_body_button_display);
                    box_body.appendChild(box_body_button_relay);
                    box_body.appendChild(box_body_button_temp_dht11);
                    box_body.appendChild(box_body_button_temp_dallas);
                    box_body.appendChild(box_body_button_ir);
                    box_body.appendChild(box_body_button_co);
                    box_body.appendChild(box_body_button_humidity);
                    box_body.appendChild(box_body_button_led);

                    sensors_list.appendChild(box_wrapper);
                });
                var buttons = document.querySelectorAll('#clients .initialize');
                Array.from(buttons).forEach(function (button) {
                    button.addEventListener('click', function (e) {
                        console.log(e.target.id);
                        //console.log(e.target.parentElement.parentElement.parentElement.id);
                        var topic = e.target.parentElement.parentElement.parentElement.id;
                        initialize_sensor(session, topic, JSON.stringify({"sensor": e.target.id, "new": true, "pins": {"GPIO": 34, "name": null, "threshold": 1200}}));
                    })

                });

                var buttons2 = document.querySelectorAll('#clients .initialize_relay');
                Array.from(buttons2).forEach(function (button) {
                    button.addEventListener('click', function (e) {
                        console.log(e.target.id);
                        //console.log(e.target.parentElement.parentElement.parentElement.id);
                        var topic = e.target.parentElement.parentElement.parentElement.id;
                        var pins = {
                            '1': {'GPIO': 15, 'state': false, 'name': null},
                            '2': {'GPIO': 13, 'state': false, 'name': null},
                            '3': {'GPIO': 14, 'state': false, 'name': null},
                            '4': {'GPIO': 2, 'state': false, 'name': null},
                            '5': {'GPIO': 0, 'state': false, 'name': null},
                            '6': {'GPIO': 16, 'state': false, 'name': null},
                            '7': {'GPIO': 25, 'state': false, 'name': null},
                            '8': {'GPIO': 26, 'state': false, 'name': null}
                        };
                        initialize_sensor(session, topic, JSON.stringify({"sensor": e.target.id, "new": true, "pins": pins}));
                    })

                });
            }
        });
}

function initialize_sensor(session, topic, sensor){
    //console.log(topic, sensor);
    session.call('mqtt_publish', ['initialize/' + topic, sensor]).then(
        function (response) {
            console.log(response)

        }
    )
}

/*
 relay pins
 ch1 = 15
 ch2 = 13
 ch3 = 14
 ch4 = 2
 ch5 = 0
 ch6 = 16
 ch7 = 25
 ch8 = 26

 {
 '1': {'GPIO': 15, 'state': False, 'name': null},
 '2': {'GPIO': 13, 'state': False, 'name': null},
 '3': {'GPIO': 14, 'state': False, 'name': null},
 '4': {'GPIO': 2, 'state': False, 'name': null},
 '5': {'GPIO': 0, 'state': False, 'name': null},
 '6': {'GPIO': 16, 'state': False, 'name': null},
 '7': {'GPIO': 25, 'state': False, 'name': null},
 '8': {'GPIO': 26, 'state': False, 'name': null},
 }
 must send on relay initialization

 *//**
 * Created by marko on 22.11.2017.
 */
